//
//  NotificationClass.h
//  IosApplicationFrameworkProject
//
//  Created by test on 07/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDefaultPreferences.h"
#import "AppConstants.h"
#import <UIKit/UIKit.h>
#import "LocalModel.h"
#import "NewsListModel.h"
#import "ApplicationController.h"
#import "HomeScreenViewController.h"



@interface NotificationClass : NSObject
{
    LocalModel *localModel;
    NewsListModel *newsListModel;
    NSMutableArray *allNewsDataArray;
    NSString* newsId;
    UILocalNotification *localNotification;
    NSThread *timerThread ;
}

@property (strong,nonatomic)HomeScreenViewController *homeScreenViewController;
-(void)switchToLocalNotificationNews:(UILocalNotification *)notification;
-(void)initialize;
-(void)checkNotificationSwitchState;
@end
