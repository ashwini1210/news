//
//  LocalModel.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#ifndef __LOCAL_MODEL_H__
#define __LOCAL_MODEL_H__

#import <Foundation/Foundation.h>
#import "IModel.h"
#import "NewsListModel.h"

@class NotificationClass;

@interface LocalModel : NSObject<IModel>
{
    //declare your variable here
}

@property (nonatomic,strong) NewsListModel *newsListModel;
@property (nonatomic,strong) NotificationClass *notificationClass;
@end

#endif

