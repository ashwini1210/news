//
//  NewsListModel.m
//  IosApplicationFrameworkProject
//
//  Created by test on 10/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "NewsListModel.h"
#import "NewsDao.h"
#import "AppConstants.h"

@implementation NewsListModel

/*
 * Method to initialize parameters
 */
-(void)initialize{
    
    [self initializeArrays];
    [self readNewsJsonTxtFile];
}

-(void)initializeArrays{
    self.allNewsDataArray = [[NSMutableArray alloc]init];
    self.topStoryDataArray = [[NSMutableArray alloc]init];
    self.trendingDataArray = [[NSMutableArray alloc]init];
    self.indiaDataArray= [[NSMutableArray alloc]init];
    self.buisnessDataArray= [[NSMutableArray alloc]init];
    self.politicsDataArray= [[NSMutableArray alloc]init];
    self.sportsDataArray= [[NSMutableArray alloc]init];
    self.technologyDataArray= [[NSMutableArray alloc]init];
    self.startupDataArray = [[NSMutableArray alloc]init];
    self.entertainmentDataArray = [[NSMutableArray alloc]init];
    self.internationalDataArray = [[NSMutableArray alloc]init];
    self.healthDataArray = [[NSMutableArray alloc]init];
    self.bookMarkArray = [[NSMutableArray alloc]init];
     self.unreadNewsArray = [[NSMutableArray alloc]init];

}

/*
 *Method to read Json txt file data.
 */
- (void)readNewsJsonTxtFile
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"NewsListAPI" ofType:@"txt"];
    NSString *stringContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [stringContent dataUsingEncoding:NSUTF8StringEncoding];
    [self onResponse:data];
}



-(void)onResponse:(NSData *)responseData{
    if(responseData!=nil){
        [self parseResponseData:responseData];
    }
    [self informView];
    
}
/*
 * Parse the response data here.
 */
-(void)parseResponseData:(NSData*)responseData
{
    NSError* error;
    NSDictionary* jsonData = [NSJSONSerialization
                              JSONObjectWithData:responseData
                              options:kNilOptions
                              error:&error];
    
    if(jsonData!=nil){
        [self parseErrorCodeAndMessage:jsonData];
        [self parseNewsListData:jsonData];
    }
}

/*
 * Method to parse error message
 */
-(void)parseErrorCodeAndMessage :(NSDictionary*)jsonResponse{
    if([jsonResponse valueForKey:@"error_code"]!=[NSNull null]){
        self.errorCode = [[jsonResponse valueForKey:@"error_code"]integerValue];
    }
    NSLog(@"error code: %ld", (long)self.errorCode);
    if([jsonResponse valueForKey:@"error_message"]!=[NSNull null]){
        
        self.errorMessage = [jsonResponse valueForKey:@"error_message"];
    }
    NSLog(@"error message :  %@",self.errorMessage);
}

/*
 * Method to parse response data
 */
-(void)parseNewsListData : (NSDictionary*)jsonNewsListDictionary{
    if([jsonNewsListDictionary objectForKey:@"result"]!=[NSNull null]) {
        
        NSDictionary* resultJsonDictionary = [jsonNewsListDictionary objectForKey:@"result"];
        
        if(resultJsonDictionary!=nil){
            
            //Check 'news' key is present in response
            if ([resultJsonDictionary objectForKey:@"news"]!=[NSNull null]) {
                
                NSArray* tempNewsListJsonArray = [resultJsonDictionary objectForKey:@"news" ];
                
                int newsListCount = [tempNewsListJsonArray count];
                
                for(int newsListIndex=0; newsListIndex<newsListCount; newsListIndex++) {
                    
                    NSDictionary *newsListItem =  [tempNewsListJsonArray objectAtIndex:newsListIndex];
                    
                    NewsDao *newsDao =  [self parseNewsDao:newsListItem];
                    
                    [self setCategoryDataInArrays:newsDao];
                    
                    //  [self.newsListDataArray addObject:newsDao];
                    
                }
            }
            
        }
    }
}

/*
 * Method to parse NewsDao response
 */
-(NewsDao*)parseNewsDao : (NSDictionary *)newsListItem{
    
    NewsDao *newsDao =[[NewsDao alloc]init];
    
    if ([newsListItem valueForKey:@"news_id"]!=[NSNull null]) {
        newsDao.newsId = [[newsListItem valueForKey:@"news_id"]integerValue];
        
    }
    if ([newsListItem valueForKey:@"image_url"]!=[NSNull null]) {
        newsDao.imageURL = [newsListItem valueForKey:@"image_url"];
        
    }
    if ([newsListItem valueForKey:@"news_heading"]!=[NSNull null]) {
        newsDao.newsHeading = [newsListItem valueForKey:@"news_heading"];
        
    }
    if ([newsListItem valueForKey:@"news_content"]!=[NSNull null]) {
        newsDao.newsContent = [newsListItem valueForKey:@"news_content"];
        
    }
    if ([newsListItem valueForKey:@"author_name"]!=[NSNull null]) {
        newsDao.authorName = [newsListItem valueForKey:@"author_name"];
        
    }
    if ([newsListItem valueForKey:@"news_category"]!=[NSNull null]) {
        newsDao.newsCategoryType = [newsListItem valueForKey:@"news_category"];
        
    }
    
    if ([newsListItem valueForKey:@"day"]!=[NSNull null]) {
        NSString *wholeDayDescription = [newsListItem valueForKey:@"day"];
        
        if (wholeDayDescription!=nil) {
            
            //Split day data into day and date
            NSArray *dayDescription = [wholeDayDescription componentsSeparatedByString:@","];
            
            
            //Date of pick up
            if (dayDescription != nil && dayDescription.count>0) {
                newsDao.newsDay=[dayDescription objectAtIndex:0];
            }
            
            //Time of pick up
            if (dayDescription != nil && dayDescription.count>1) {
                newsDao.newsDate =[dayDescription objectAtIndex:1];
                
            }
            
        }
        
    }
    if ([newsListItem valueForKey:@"link"]!=[NSNull null]) {
        newsDao.descriptiveNewsLink = [newsListItem valueForKey:@"link"];
        
    }
    if ([newsListItem valueForKey:@"more_at"]!=[NSNull null]) {
        newsDao.moreNewsAt = [newsListItem valueForKey:@"more_at"];
        
    }
    if ([newsListItem valueForKey:@"video_id"]!=[NSNull null]) {
        newsDao.videoId = [newsListItem valueForKey:@"video_id"];
        
    }
   
    return newsDao;
    
}

/*
 * Method to set data into categoryArray according to news category.
 */
-(void)setCategoryDataInArrays:(NewsDao*)newsDao{
    
    [self.allNewsDataArray addObject:newsDao];
    
    if ([newsDao.newsCategoryType isEqual:TOP_STORIES_CATEGORY_TYPE]) {
        
        [self.topStoryDataArray addObject:newsDao];
    }
    else if ([newsDao.newsCategoryType isEqual:TRENDING_CATEGORY_TYPE]){
        
        [self.trendingDataArray addObject:newsDao];
    }
    else if ([newsDao.newsCategoryType isEqual:INDIA_CATEGORY_TYPE]){
        
        [self.indiaDataArray addObject:newsDao];
    }
    
    else if ([newsDao.newsCategoryType isEqual: BUISNESS_CATEGORY_TYPE]){
        
        [self.buisnessDataArray addObject:newsDao];
    }
    
    else if ([newsDao.newsCategoryType isEqual:POLITICS_CATEGORY_TYPE]){
        
        [self.politicsDataArray addObject:newsDao];
    }
    
    else if ([newsDao.newsCategoryType isEqual: SPORTS_CATEGORY_TYPE]){
        
        [self.sportsDataArray addObject:newsDao];
    }
    
    else if ([newsDao.newsCategoryType isEqual: TECHNOLOGY_CATEGORY_TYPE]){
        
        [self.technologyDataArray addObject:newsDao];
    }
    else if ([newsDao.newsCategoryType isEqual:STARTUP_CATEGORY_TYPE]){
        
        [self.startupDataArray addObject:newsDao];
    }
    
    else if ([newsDao.newsCategoryType isEqual:ENTERTAINMENT_CATEGORY_TYPE]){
        
        [self.entertainmentDataArray addObject:newsDao];
    }
    
    else if ([newsDao.newsCategoryType isEqual:INTERNATIONAL_CATEGORY_TYPE]){
        
        [self.internationalDataArray addObject:newsDao];
    }
    else if ([newsDao.newsCategoryType isEqual:HEALTH_CATEGORY_TYPE]){
        
        [self.healthDataArray addObject:newsDao];
    }
    
}

/*
 * Method to get Categories Array
 */
-(NSMutableArray*)getCategoriesDataArray:(NSString*)newsCategory{
    
    if ([newsCategory isEqual:TOP_STORIES_CATEGORY_TYPE]) {
        return self.topStoryDataArray;
    }
    else if ([newsCategory isEqual:TRENDING_CATEGORY_TYPE]){
        return self.trendingDataArray ;
    }
    else if ([newsCategory isEqual:INDIA_CATEGORY_TYPE]){
        return self.indiaDataArray ;
    }
    
    else if ([newsCategory isEqual:BUISNESS_CATEGORY_TYPE]){
        return self.buisnessDataArray;
    }
    
    else if ([newsCategory isEqual:POLITICS_CATEGORY_TYPE]){
        return self.politicsDataArray ;
    }
    
    else if ([newsCategory isEqual:SPORTS_CATEGORY_TYPE]){
        return self.sportsDataArray ;
    }
    
    else if ([newsCategory isEqual:TECHNOLOGY_CATEGORY_TYPE]){
        return self.technologyDataArray ;
    }
    else if ([newsCategory isEqual:STARTUP_CATEGORY_TYPE]){
        return self.startupDataArray ;
    }
    
    else if ([newsCategory isEqual:ENTERTAINMENT_CATEGORY_TYPE]){
        return self.entertainmentDataArray;
    }
    
    else if ([newsCategory isEqual:INTERNATIONAL_CATEGORY_TYPE]){
        return self.internationalDataArray ;
    }
    else if ([newsCategory isEqual:HEALTH_CATEGORY_TYPE]){
        return self.healthDataArray ;
    }
    else if ([newsCategory isEqual:ALL_NEWS_CATEGORY_TYPE]){
        return self.allNewsDataArray ;
    }
    return self.allNewsDataArray;
    
}



@end
