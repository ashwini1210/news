//
//  TestScreenViewController.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/23/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface TestScreenViewController : UIViewController<AbstractViewController>
@property (weak, nonatomic) IBOutlet UIButton *button;
- (IBAction)onButtClick:(id)sender;
- (IBAction)goToNextScreenButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;

@end
