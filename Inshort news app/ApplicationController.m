//
//  ApplicationController.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "ApplicationController.h"
#import "AppDefines.h"
#import <Foundation/Foundation.h>
#import "AppConstants.h"
#import "ViewFactory.h"



@implementation ApplicationController


#pragma mark - singleton method

+(ApplicationController *)getInstance
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    //static id sharedObject = nil;  //if you're not using ARC
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] init];
        //sharedObject = [[[self alloc] init] retain]; // if you're not using ARC
    });
    return sharedObject;
}



/**
 * Constructor of ApplicationController
 */
- (id)init
{
    self = [super init];
    if (self) {
        // Create the instance of ModelFacade
        self->modelfacade = [[ModelFacde alloc] init];
        self->uicontroller =[[UIController alloc] init];
        self->viewFactory = [[ViewFactory alloc] init];
        
        NSLog(@"modelfacade Object Created");
    }
    return self;
}

//-(void)setMainView:(UIView*)uiView
//{
//    [self->uicontroller setMainView:uiView];
//
//}

/**
 * This Function will get called from MainActivity or Any Activity which is
 * going to display first screen after launching this application
 *
 * @param activity
 */
-(void) initialize
{
    [self->modelfacade initialize];
    
    [self->uicontroller initialize];
    
    
    NSLog(@"uicontroller Object Created by calling getInstance");
    
    
    
    NSLog(@"ViewFactory Object Created by calling getInstance");
    
    
}

/*
 Launch the first screen of the app, for example Splash screen
 */
- (void) launchFirstScreen{
    
    [self launchSplashScreen];
}
//
-(void) launchSplashScreen
{
    [self handleEvent:EVENT_ID_SPLASH_SCREEN];
    
}

-(void)launchHomeScreen{
    [self handleEvent:EVENT_ID_HOME_SCREEN];
}


/**
 * This function must get called before exiting the application. otherwise
 * there will be chances for memory leakages.
 */
-(void) destroy
{
    if (self->modelfacade != nil)
    {
        // Destroy the ModelFacade
        [self->modelfacade destory];
        
        
        // set modelFacade to null for garbage collection
        self->modelfacade = nil;
    }
    
    if (self->uicontroller != nil)
    {
        [self->uicontroller destory];
        
        self->uicontroller = nil;
    }
    
    if (self->viewFactory != nil)
    {
        [self->viewFactory releaseAllScreen];
        
        self->viewFactory = nil;
    }
    
    
    [super destory];
}

-(void) disable
{
    if (self->uicontroller != nil)
    {
        [self->uicontroller disbale];
    }
    [super disbale];
}

-(void) hideNotify
{
    if (self->uicontroller != nil)
    {
        [self->uicontroller hideNotify];
    }
    [super hideNotify];
}

-(void) showNotify
{
    if (self->uicontroller != nil)
    {
        [self->uicontroller showNotify];
    }
    [super showNotify];
}

-(void) handleEvent:(int)eventId
{
    [super handleEvent:eventId];
    [self handleEvent:eventId andeventObject:nil];
}

-(void) handleEvent:(int)eventId andeventObject:(NSObject *)eventObject
{
    //[super havdleEvent:eventId andeventObject:NULL];
    
    [super handleEvents:eventId AndEventJoin:nil];
    
    switch (eventId) {
            //define cases to handle particular event.
          
        case EVENT_ID_TEST_SCREEN:{
            [self->uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_TEST_SCREEN viewFactory:viewFactory];
            break;
        }
        case EVENT_ID_ROOT_NAVIGATION_SCREEN:{
            [self->uicontroller pushScreen:VCT_ROOT_NAVIGATION_CONTROLLER screenId:VF_ROOT_NAVIGATION_SCREEN viewFactory:viewFactory];
            break;
        }
        case EVENT_ID_ROOT_NAVIGATION_CHILD_SCREEN1:{
            [self->uicontroller pushScreen:VCT_CHILD_NAVIGATION_CONTROLLER screenId:VF_ROOT_NAVIGATION_CHILD_SCREEN1 viewFactory:viewFactory];
            break;
        }
        case EVENT_ID_ROOT_NAVIGATION_CHILD_SCREEN2:{
            [self->uicontroller pushScreen:VCT_CHILD_NAVIGATION_CONTROLLER screenId:VF_ROOT_NAVIGATION_CHILD_SCREEN2 viewFactory:viewFactory];
            break;
        case EVENT_ID_TEST1_VIEW_CONTROLLER:{
            [self->uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_TEST1_VIEW_CONTROLLER viewFactory:viewFactory];
            break;
            
        }case EVENT_ID_FINISH_ROOT_NAVIGATION_SCREEN:{
            [self->uicontroller popScreen:VCT_ROOT_NAVIGATION_CONTROLLER];
            break;
        }
        case EVENT_ID_FINISH_CHILD_NAVIGATION_SCREEN:{
            [self->uicontroller popScreen:VCT_CHILD_NAVIGATION_CONTROLLER];
            break;
        }
        case EVENT_ID_ON_SCREEN_FINISH:{
            [self->uicontroller onScreenPoped];
            break;
        }
            
        case EVENT_ID_CUSTOM_TABLE_VIEW_SCREEN:{
            [self->uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_CUSTOM_TABLE_VIEW_SCREEN viewFactory:viewFactory];
            break;
        }
        case EVENT_ID_CUSTOM_PAGE_VIEW_SCREEN:{
            [self->uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_CUSTOM_PAGE_VIEW_SCREEN viewFactory:viewFactory];
            break;
        }
        case EVENT_ID_CUSTOM_COLLECTION_VIEW_SCREEN:{
            [self->uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_CUSTOM_COLLECTION_VIEW_SCREEN viewFactory:viewFactory];
            break;
        }
            
            
            /*
             * Actual Event Id's
             */
        case EVENT_ID_HOME_SCREEN :
            {
                
                [uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_HOME_SCREEN viewFactory:viewFactory eventObject:eventObject];
                break;
            }
        case EVENT_ID_NEWS_WEB_VIEW_SCREEN :
            {
                
                [uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_NEWS_WEB_VIEW_SCREEN viewFactory:viewFactory eventObject:eventObject];
                break;
            }
        case EVENT_ID_CATEGORIES_VIEW_SCREEN :
            {
                
                [uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_CATEGORIES_VIEW_SCREEN viewFactory:viewFactory eventObject:eventObject];
                break;
            }
        case EVENT_ID_SETTING_SCREEN :
            {
                
                [uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_SETTING_SCREEN viewFactory:viewFactory eventObject:eventObject];
                break;
            }
        case EVENT_ID_DETAIL_NEWS_PAGER_SCREEN :
            {
                
                [uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_DETAIL_NEWS_PAGER_SCREEN viewFactory:viewFactory eventObject:eventObject];
                break;
            }
        case EVENT_ID_SPLASH_SCREEN :
            {
                [uicontroller pushScreen:VCT_VIEW_CONTROLLER screenId:VF_SPLASH_SCREEN viewFactory:viewFactory eventObject:eventObject];
                break;
            }

        
        case EVENT_ID_FINISH_SCREEN:{
            
            [self->uicontroller popScreen:VCT_VIEW_CONTROLLER];
            break;
        }
        case EVENT_ID_FINISH_TOP_SCREENS:{
            NSNumber *screenIdNum = (NSNumber*) eventObject;
            [self->uicontroller clearUIStackToScreen:[screenIdNum intValue]];
            break;
        }
            
        }
    }
}

/**
 * Get the reference of ModelFacade
 *
 * @return ModelFacade
 */
-(ModelFacde *)getModelFacade
{
    return modelfacade;
}

/**
 * Get the reference of UIController
 *
 * @return
 */
-(UIController *)getUiConroller
{
    return uicontroller;
}

-(void) closeStackedActivity
{
    
}

-(void) closeApplication
{
    
}

@end
