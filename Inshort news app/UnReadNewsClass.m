//
//  UnreadNewsCLass.m
//  IosApplicationFrameworkProject
//
//  Created by test on 11/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "UnReadNewsClass.h"
#import "Utils.h"

@implementation UnReadNewsClass

-(id)init{
    self = [super init];
    [self initializeInstances];
    return self;
}

/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    
    localModel = [[[ApplicationController getInstance] getModelFacade] getLocalModel];
    newsListModel = localModel.newsListModel;
    
}

/*
 * Method to get UnreadNews array
 */
-(NSMutableArray*)getUnreadNews{
    
    NSMutableArray *allNewsArray = newsListModel.allNewsDataArray;
    
    for (int i=0; i<[allNewsArray count]; i++) {
        
        NewsDao *newsDao = [allNewsArray objectAtIndex:i];
        
        //If isReadNews = false then it is Unread News so keep it's deo in array
        if (!newsDao.isReadNews && ![newsListModel.unreadNewsArray containsObject:newsDao]) {
            [newsListModel.unreadNewsArray addObject:newsDao];
        }
        
        newsDao = nil;
    }
   
    return newsListModel.unreadNewsArray;
}

/*
 * Once news are read from unread news array then shift it into read news array. And remove same news form unread array
 */
-(void)markAsReadNews:(NewsDao*)newsDao{
    BOOL isReadNews = newsDao.isReadNews;
    
    //If isReadNews = true then it is Read News so remove deo from array
    if (isReadNews) {
        [newsListModel.unreadNewsArray removeObject:newsDao];
       // NSLog(@"Unread news count after removing read news %d",[newsListModel.unreadNewsArray count]);
    }
    
}

@end



    
    

