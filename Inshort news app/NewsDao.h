//
//  NewsDao.h
//  IosApplicationFrameworkProject
//
//  Created by test on 10/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 * Dao to store News data
 */
@interface NewsDao : NSObject

/*
 * Variable for news id
 */
@property NSInteger newsId;
/*
 * Variable for image URL
 */
@property (nonatomic,strong) NSString *imageURL;
/*
 * Variable for news content
 */
@property (nonatomic,strong) NSString *newsContent;
/*
 * Variable for author name
 */
@property (nonatomic,strong) NSString *authorName;
/*
 * Variable for news day
 */
@property (nonatomic,strong) NSString *newsDay;
/*
 * Variable for news date
 */
@property (nonatomic,strong) NSString *newsDate;
/*
 * Variable for news link
 */
@property (nonatomic,strong) NSString *descriptiveNewsLink;
/*
 * Variable for news heading
 */
@property (nonatomic,strong) NSString *newsHeading;
/*
 * Variable for news category
 */
@property (nonatomic,strong) NSString *newsCategoryType;

/*
 * Variable for news category
 */
@property BOOL isReadNews;

/*
 * Variable for more news text
 */
@property (nonatomic,strong) NSString *moreNewsAt;

/*
 * Variable for video ID
 */
@property (nonatomic,strong) NSString *videoId;


@end
