//
//  GetAllUsersModel.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/27/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "GetAllUsersModel.h"
#import "AppConstants.h"
@implementation GetAllUsersModel

-(void) initialize{
    [super initialize];
    [self getAllUsersData];
}

-(void)getAllUsersData{
    [httpRequestHandlerImpl setServiceURL:URL_CHECK_GET_ALL_USERS];
    [httpRequestHandlerImpl setMethodType:HTTP_GET];
    [httpRequestHandlerImpl setHeaders:@"Authorization" value:[self encodeAuthenticationWithBase64]];
//    responseData = [httpRequestHandlerImpl execute];
//    NSLog(@"Response: %@ ",responseData);
//    //    NSLog(@" Error %@", error);
//    NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//    NSLog(@" Response String: %@",str);
    
}

-(NSString*) encodeAuthenticationWithBase64{
    //    String auth = Base64.encodeToString("username:password".getBytes(),Base64.DEFAULT);
    NSData *nsdata = [@"ashish:123456"
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    NSLog(@"Authentication Base 64: %@ ",base64Encoded);
    NSLog(@"AUth: %@",[@"Basic "stringByAppendingString:base64Encoded]);
    return [@"Basic "stringByAppendingString:base64Encoded];
}
@end
