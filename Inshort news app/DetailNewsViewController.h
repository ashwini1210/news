//
//  DetailNewsViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 04/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "BaseViewController.h"
#import "NewsDao.h"
#import "UserDefaultPreferences.h"

@interface DetailNewsViewController : BaseViewController
{
    UserDefaultPreferences *userDefaultPreferences;
}
@property (strong, nonatomic) IBOutlet UILabel *labelNewsDate;
@property (strong, nonatomic) IBOutlet UILabel *labelNewsHeadline;
@property (strong, nonatomic) IBOutlet UIImageView *newsImageView;
@property (strong, nonatomic) IBOutlet UITextView *detailNewsText;
@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) NewsDao *newsDao;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *secondView;
@property (strong, nonatomic) IBOutlet UIView *firstView;
@property (strong, nonatomic) IBOutlet UIView *moreAtParentView;
@property (strong, nonatomic) IBOutlet UIView *omreAtVideoView;
@property (strong, nonatomic) IBOutlet UIView *moreAtWebView;
@property (strong, nonatomic) IBOutlet UILabel *videoAtCOnstantText;
@property (strong, nonatomic) IBOutlet UILabel *videoAtYoutubeText;
- (IBAction)playVideoButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *moreAtConstantText;
@property (strong, nonatomic) IBOutlet UILabel *moreAtWebText;
- (IBAction)moreAtWebNewsButtonClickEvent:(id)sender;
@property (strong, nonatomic) NSString* youTubeVideoId;

@end
