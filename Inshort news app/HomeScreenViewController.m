//
//  HomeScreenViewControllerAdapter.m
//  IosApplicationFrameworkProject
//
//  Created by test on 08/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "HomeScreenViewController.h"
#import "ApplicationController.h"
#import "ListHomeScreenTableViewCell.h"
#import "Utils.h"
#import "HomeScreenViewControllerAdapters.h"
#import "NotificationClass.h"

@interface HomeScreenViewController ()

@end

@implementation HomeScreenViewController

BOOL isTitleBarVisible = true;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeInstances];
    [self setAdapterToHomeScreenViewController];
    [self setHomeScreenViewsOnModeState];
}

-(void)viewWillAppear:(BOOL)animated{
    //If Pager Mode is on then Apply Tap gesture to view.
    if (![self isListModeOn]){
        [self tapGestureRecognizer];
    }
    else{
        //If list Mode is on then remove Tap gesture from view.
        [self.contentView removeGestureRecognizer:tap];
        self.newsTableView.scrollEnabled = true;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [self.contentView setHidden:NO];
    localModel.notificationClass.homeScreenViewController = self;
}


/*
 * Method to Recognize tap Gesture.
 */
-(void)tapGestureRecognizer
{
    
    tap  = [[UITapGestureRecognizer alloc]
            initWithTarget:self
            action:@selector(displayTitleBar)];
    [self.contentView addGestureRecognizer:tap];
    
}

/*
 * Mehtod to hide and show title bar.
 */
-(void)displayTitleBar{
    if(!isTitleBarVisible)
    {
        [self showTitleBar];
        
    }
    else
    {
        [self hideTitleBar];
    }
    
}

/*
 * Mehtod to Hide Title Bar.
 */
-(void)hideTitleBar{
    
    self.parentTitleBarView.frame =  CGRectMake(0,26,self.parentTitleBarView.frame.size.width, self.parentTitleBarView.frame.size.height);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.parentTitleBarView.frame =  CGRectMake(0, -54,self.parentTitleBarView.frame.size.width, self.parentTitleBarView.frame.size.height);
    }];
    
    isTitleBarVisible=false;
    
}


/*
 * Mehtod to Show Title Bar.
 */
-(void)showTitleBar{
    
    self.parentTitleBarView.frame =  CGRectMake(0, -54,self.parentTitleBarView.frame.size.width, self.parentTitleBarView.frame.size.height);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.parentTitleBarView.frame =  CGRectMake(0, 26,self.parentTitleBarView.frame.size.width, self.parentTitleBarView.frame.size.height);
    }];
    self.parentTitleBarView.hidden=NO;
    isTitleBarVisible=true;
}


//Scrolling is enable only on list Mode so when scrolling begin hide tab bar
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if(isTitleBarVisible)
    {
        [self hideTitleBar];
        
    }
    
    
}

// when scrolling end show tab bar
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(!isTitleBarVisible)
    {
        [self showTitleBar];
        
    }
    
}


/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    
    customAlertView =[CustomAlertView createCustomAlertView:CUSTOM_ALERT_ERROR_TITLE alertMessage:@"" leftButtonTitle:CUSTOM_ALERT_OK_BUTTON_TEXT rightButtonTitle:nil delegate:self identifier:1];
    localModel = [[[ApplicationController getInstance] getModelFacade] getLocalModel];
    self.newsListDataArray = [[NSMutableArray alloc]init];
    self.newsListModel = [[NewsListModel alloc]init];
    
    userDefaultPreferences = [UserDefaultPreferences getInstance];
    
    if (localModel!=nil) {
        localModel.newsListModel = self.newsListModel;
        
    }
    
    [self.newsListModel registerview:self];
    [self.newsListModel initialize];
    
}

/*
 * Method to initialize Unread news Class
 */
-(void)initializeUnreadClass{
    self.unReadNewsClass = [[UnReadNewsClass alloc]init];
    
}

/*
 * Method to set adaoter for home screen viewcontroller
 */
-(void)setAdapterToHomeScreenViewController{
    self.uiPageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationVertical options:nil];
    self.uiPageViewController.dataSource = self;
    [[self.uiPageViewController view] setFrame:[[self contentView] bounds]];
    homeScreenViewControllerAdapters = [self viewControllerAtIndex:0];
    homeScreenViewControllerAdapters.homeScreenViewController = self;
    NSArray *viewControllers = [NSArray arrayWithObject:homeScreenViewControllerAdapters];
    [self.uiPageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:self.uiPageViewController];
    [[self contentView] addSubview:[self.uiPageViewController view]];
    [self.uiPageViewController didMoveToParentViewController:self];
    viewControllers = nil;
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


//UIPAGER VIEW METHOD
/*
 * Method to initialize HomeScreenViewControllerAdapters and set data into its views
 */
- (HomeScreenViewControllerAdapters *)viewControllerAtIndex:(NSUInteger)index {
    @autoreleasepool {
        
        HomeScreenViewControllerAdapters *homeScreenViewControllerAdapters = [[HomeScreenViewControllerAdapters alloc] initWithNibName:@"HomeScreenViewControllerAdapters" bundle:nil];
        homeScreenViewControllerAdapters.homeScreenViewController = self;
        homeScreenViewControllerAdapters.pageIndex = index;
        [self setDataIntoPagerViews:homeScreenViewControllerAdapters index:index];
        return homeScreenViewControllerAdapters;
    }
}

/*
 * On click of notification, open respective index pager
 */
-(void)onNotificationClicked:(NSUInteger)allNewsIndex {
    
    [self.newsListDataArray removeAllObjects];
    [self.newsListDataArray addObjectsFromArray:self.newsListModel.allNewsDataArray];
    self.newsListModel.currentSelectedCategory = ALL_NEWS_CATEGORY_TYPE;
    [self refreshData];
    //then scroll to page having same index.
    [self scrollViewToTopIndex:allNewsIndex];
    
}


//UIPAGER VIEW DELIGATE METHOD
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(HomeScreenViewControllerAdapters *)viewController pageIndex];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}


//UIPAGER VIEW DELIGATE METHOD
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(HomeScreenViewControllerAdapters *)viewController pageIndex];
    
    index++;
    
    if (index == [self.newsListDataArray count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}


//UIPAGER VIEW DELIGATE METHOD
/*
 * Method to set view pager count
 */
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [self.newsListDataArray count];
}


//UIPAGER VIEW DELIGATE METHOD
/*
 * Method to set first vire pager index
 */
- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

//UIPAGER VIEW METHOD
/*
 * Method to set data into pager views
 */
-(void)setDataIntoPagerViews:(HomeScreenViewControllerAdapters *)homeScreenViewControllerAdapters index:(NSUInteger) index{
    @autoreleasepool {
        
        int newsindex = (int)index;
        NewsDao *newsDao = [self getNewsByIndex:newsindex];
        homeScreenViewControllerAdapters.newsDo = newsDao;
    }
}


//UITABLE VIEW DELIGATE METHOD
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.newsListDataArray count];
}


//UITABLE VIEW DELIGATE METHOD
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *listTableIdentifier = @"ListHomeScreenTableViewCell";
    
    ListHomeScreenTableViewCell *cell = (ListHomeScreenTableViewCell *)[tableView dequeueReusableCellWithIdentifier:listTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListHomeScreenTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    index = indexPath;
    [self setDataIntoTableView:cell index:indexPath.row];
    
    return cell;
}


//UITABLE VIEW DELIGATE METHOD
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    NSLog(@"Selected Row %ld",(long)indexPath.row);
    NSMutableDictionary *newsDataDictionary = [[NSMutableDictionary alloc]init];
    [newsDataDictionary setValue:[NSNumber numberWithInt:indexPath.row] forKey:SELECTED_CELL_INDEX_KEY];
    [newsDataDictionary setObject:self.newsListDataArray forKey:SELECTED_CATEGORY_ARRAY];
    [[ApplicationController getInstance]handleEvent:EVENT_ID_DETAIL_NEWS_PAGER_SCREEN andeventObject:newsDataDictionary];
}



//UITABLE VIEW  METHOD
/*
 * Method to set data into views
 */
-(void)setDataIntoTableView:(ListHomeScreenTableViewCell*)listTableViewCell index:(int)index{
    @autoreleasepool {
        
        NewsDao *newsDao = [self getNewsByIndex:index];
        if (newsDao!=nil) {
            
            if (newsDao.imageURL!=nil) {
                listTableViewCell.newsImageView.image = [UIImage imageNamed:newsDao.imageURL];
            }
            
            if (newsDao.newsHeading!=nil ) {
                
                listTableViewCell.newsHeadlinesLabel.text = newsDao.newsHeading;
            }
            
            if (newsDao.newsDay!=nil) {
                listTableViewCell.newsDateLabel.text = newsDao.newsDay;
            }
            
            newsDao.isReadNews = true;
            
        }
        
        [self.unReadNewsClass markAsReadNews:newsDao];
    }
}


-(void)update{
    
    if (self.newsListModel.errorCode==SUCCESS_CODE) {
        [self.newsListDataArray addObjectsFromArray:[self.newsListModel getCategoriesDataArray:ALL_NEWS_CATEGORY_TYPE]];
        [self initializeUnreadClass];
        [self refreshData];
    }
}


/*
 * Method to refresh data
 */
-(void)refreshData{
    
    //If switchListModeState = true (i.e list mode is on) then refresh table view
    if ([self isListModeOn]) {
        
        [self.newsTableView reloadData];
    }
    
    //If switchListModeState = false (i.e list mode is off) then refresh pager view
    else{
        
        [self.uiPageViewController reloadInputViews];
    }
    
}

- (IBAction)menuButtonClickEvent:(id)sender {
    [[ApplicationController getInstance]handleEvent:EVENT_ID_CATEGORIES_VIEW_SCREEN andeventObject:self];
}

- (IBAction)upScrollButtonClickEvent:(id)sender {
    
}

- (IBAction)settingButtonClickEvent:(id)sender {
    [[ApplicationController getInstance]handleEvent:EVENT_ID_SETTING_SCREEN andeventObject:self];
}

/*
 * Method to set category data in pager. When user select any button from category, as per selected category data will be set into pager.
 */
-(void)setCategoryDataInPager:(NSString*)selectedCategory{
    @autoreleasepool {
        
        
        self.topScreenFinish = VF_CATEGORIES_VIEW_SCREEN;
        
        if ([selectedCategory isEqual:TOP_STORIES_CATEGORY_TYPE]) {
            self.newsListModel.currentSelectedCategory = TOP_STORIES_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.topStoryDataArray!=nil && [self.newsListModel.topStoryDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.topStoryDataArray];
            }
            
        }
        else if ([selectedCategory isEqual:ALL_NEWS_CATEGORY_TYPE]){
            self.newsListModel.currentSelectedCategory = ALL_NEWS_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.allNewsDataArray!=nil && [self.newsListModel.allNewsDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.allNewsDataArray];
            }
            
        }
        else if ([selectedCategory isEqual:TRENDING_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = TRENDING_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil ) && (self.newsListModel.trendingDataArray!=nil && [self.newsListModel.trendingDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.trendingDataArray];
            }
            
        }
        else if ([selectedCategory isEqual:INDIA_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = INDIA_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil ) && (self.newsListModel.indiaDataArray!=nil && [self.newsListModel.indiaDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.indiaDataArray];
            }
            
        }
        
        else if ([selectedCategory isEqual: BUISNESS_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = BUISNESS_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil ) && (self.newsListModel.buisnessDataArray!=nil && [self.newsListModel.buisnessDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.buisnessDataArray];
            }
            
        }
        
        else if ([selectedCategory isEqual:POLITICS_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = POLITICS_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil ) && (self.newsListModel.politicsDataArray!=nil && [self.newsListModel.politicsDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.politicsDataArray];
            }
            
        }
        
        else if ([selectedCategory isEqual: SPORTS_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = SPORTS_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil ) && (self.newsListModel.sportsDataArray!=nil && [self.newsListModel.sportsDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.sportsDataArray];
            }
            
        }
        
        else if ([selectedCategory isEqual: TECHNOLOGY_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = TECHNOLOGY_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.technologyDataArray!=nil && [self.newsListModel.technologyDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.technologyDataArray];
            }
            
        }
        else if ([selectedCategory isEqual:STARTUP_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = STARTUP_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.startupDataArray!=nil && [self.newsListModel.startupDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.startupDataArray];
            }
            
        }
        
        else if ([selectedCategory isEqual:ENTERTAINMENT_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = ENTERTAINMENT_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.entertainmentDataArray!=nil && [self.newsListModel.entertainmentDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.entertainmentDataArray];
            }
            
        }
        
        else if ([selectedCategory isEqual:INTERNATIONAL_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = INTERNATIONAL_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.internationalDataArray!=nil && [self.newsListModel.internationalDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.internationalDataArray];
            }
            
        }
        else if ([selectedCategory isEqual:HEALTH_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = HEALTH_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.healthDataArray!=nil && [self.newsListModel.healthDataArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.healthDataArray];
            }
            
        }
        else if ([selectedCategory isEqual:BOOKMARK_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = BOOKMARK_CATEGORY_TYPE;
            if ((self.newsListDataArray!=nil) && (self.newsListModel.bookMarkArray!=nil && [self.newsListModel.bookMarkArray count]!=0)) {
                [self.newsListDataArray removeAllObjects];
                [self.newsListDataArray addObjectsFromArray:self.newsListModel.bookMarkArray];
            }
            
        }
        else if ([selectedCategory isEqual:UNREAD_CATEGORY_TYPE]){
            
            self.newsListModel.currentSelectedCategory = UNREAD_CATEGORY_TYPE;
            if (self.newsListDataArray!=nil) {
                
                //Get all unread news
                NSMutableArray *unReadNewsArray = [self.unReadNewsClass getUnreadNews];
                
                if (unReadNewsArray!=nil && [unReadNewsArray count]!=0) {
                    [self.newsListDataArray removeAllObjects];
                    [self.newsListDataArray addObjectsFromArray:unReadNewsArray];
                    
                }
                
            }
            
        }
        
        
        [self refreshData];
    }
}


/*
 * Method to scroll view at 0 index when selected ant category
 */
-(void)scrollViewToTopIndex:(int)index{
    homeScreenViewControllerAdapters = [self viewControllerAtIndex:index];
    NSArray *viewControllers = @[homeScreenViewControllerAdapters];
    [self.uiPageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
}


/*
 * Gets the news for provided index if avaliable in list.
 */
-(NewsDao*)getNewsByIndex:(int)index{
    if(self.newsListDataArray!=nil && [self.newsListDataArray count]!=0){
        @try{
            return [self.newsListDataArray objectAtIndex:index];
        }@catch(NSException *nsRangeException){
            //if the array index is out of range.
            return nil;
        }
        
    }
    return nil;
}

/*
 * Method to set views into home screen as per list home is enable or disable
 */
-(void)setHomeScreenViewsOnModeState{
    
    //If list mode switch value is true then set table view in view
    if ([self isListModeOn]) {
        [self.newsTableView setHidden:NO];
        [self.newsTableView setUserInteractionEnabled:YES];
        [[self contentView] bringSubviewToFront:self.newsTableView];
        
        //If the news displaying mode is list mode then only refresh data
        [self refreshData];
        
    }
    else{
        
        //If list mode switch value is off then set pager in view
        [self.newsTableView setHidden:YES];
        [self.newsTableView setUserInteractionEnabled:NO];
        [[self contentView] bringSubviewToFront:[self.uiPageViewController view]];
        
        
        //If the news displaying mode is pager mode then refresh data
        [self refreshData];
        [homeScreenViewControllerAdapters getDataFromPreferences];
    }
    
}

/*
 * Method to show alert dialog if any of the selected category is nil.
 */
-(void)showALertDialog:(NSString*)selectedCategory{
    NSMutableArray *newsArray = [Utils getArrayFromCategory:selectedCategory];
    if ([selectedCategory isEqual:BOOKMARK_CATEGORY_TYPE]){
        if ([newsArray count]==0) {
            //Hode main content view
            [self.contentView setHidden:YES];
            [customAlertView setMessage:NO_SELECTED_BOOKMARK];
            [customAlertView show:self];
        }
    }
    
    else if ([newsArray count]==0) {
        //Hode main content view
        [self.contentView setHidden:YES];
        [customAlertView setMessage:NO_SELECTED_CATEGORY];
        [customAlertView show:self];
        
    }
}


-(void)onScreenPopedUp{
    NSLog(@"On screen poped up");
}

-(void)onTopScreenFinished{
    [self setHomeScreenViewsOnModeState];
    
    //When the top finished view controller is Category Screen then show alertview if condition is satisfied.
    if (self.topScreenFinish == VF_CATEGORIES_VIEW_SCREEN) {
        [self scrollViewToTopIndex:0];
        [self showALertDialog:self.newsListModel.currentSelectedCategory];
        self.topScreenFinish = -1;
    }
    
    
}


/*
 * Method to get list mode value from preferences
 */
-(BOOL)isListModeOn{
    NSString *listModeKey = PREF_KEY_LIST_MODE;
    BOOL isListModeOn = [userDefaultPreferences getBooleanData:listModeKey];
    listModeKey = nil;
    return isListModeOn;
}

-(void)onAlertViewLeftButtonClicked:(int)identifier{
    NSLog(@"Click on OK Button");
    
}



-(void)onAlertViewRightButtonClicked:(int)identifier{
    NSLog(@"Click on Cancel Button");
    
}


-(void)destroy{
    homeScreenViewControllerAdapters = nil;
    userDefaultPreferences = nil;
    self.newsListDataArray= nil;
    self.uiPageViewController= nil;
    self.menuButton= nil;
    self.newsCountLabel= nil;
    self.upScrollButton= nil;
    self.contentView= nil;
    self.parentTitleBarView= nil;
    self.settingButton= nil;
    self.newsTableView= nil;
    
}


@end
