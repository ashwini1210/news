//
//  BaseViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 08/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"

@interface BaseViewController : UIViewController <AbstractViewController>
/*
 * Variable to hold type of view controller's.
 */
@property int viewControllerType;
@property int screenId;

-(void)update:(int)identifier;
-(void) update;
-(void)onTopScreenFinished;
-(void)onScreenPopedUp;
-(void)destroy;
@end
