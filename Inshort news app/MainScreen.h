//
//  MainScreen.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/12/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootNavigationController.h"
#import "AbstractViewController.h"
#import "ApplicationController.h"

@interface MainScreen : UIViewController<AbstractViewController>
{
    RootNavigationController *rootNavigationController;
}
- (IBAction)clickMeButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *sampleImage;
- (IBAction)backButtonPressed:(id)sender;
-(void) onImageClick;
@end
