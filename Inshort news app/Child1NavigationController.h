//
//  Child1NavigationController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 10/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "ApplicationController.h"

@interface Child1NavigationController : UIViewController<AbstractViewController>
{
   
}
- (IBAction)goTONextScreenButtonClicked:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

@end
