//
//  UnreadNewsCLass.h
//  IosApplicationFrameworkProject
//
//  Created by test on 11/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnReadNewsClass.h"
#import "LocalModel.h"
#import "NewsListModel.h"
#import "NewsDao.h"
#import "ApplicationController.h"

@interface UnReadNewsClass : NSObject
{
    LocalModel *localModel;
    NewsListModel *newsListModel;

}

-(NSMutableArray*)getUnreadNews;
-(void)markAsReadNews:(NewsDao*)newsDao;
@end
