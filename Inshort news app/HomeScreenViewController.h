//
//  HomeScreenViewControllerAdapter.h
//  IosApplicationFrameworkProject
//
//  Created by test on 08/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "BaseViewController.h"
#import "NewsListModel.h"
#import "LocalModel.h"
#import "UserDefaultPreferences.h"
#import "ListHomeScreenTableViewCell.h"
#import "CustomAlertView.h"
#import "UnReadNewsClass.h"

@class HomeScreenViewControllerAdapters;

@interface HomeScreenViewController : BaseViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,CustomAlertViewDelegate,UIScrollViewDelegate>
{
    LocalModel *localModel;
    HomeScreenViewControllerAdapters *homeScreenViewControllerAdapters;
    UserDefaultPreferences *userDefaultPreferences;
    CustomAlertView *customAlertView;
    NSIndexPath * index;
    UITapGestureRecognizer *tap;
}

@property(strong,nonatomic)NSMutableArray* newsListDataArray;
@property (strong, nonatomic) UIPageViewController *uiPageViewController;
@property (strong, nonatomic) IBOutlet UIButton *menuButton;
- (IBAction)menuButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *newsCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *upScrollButton;
- (IBAction)upScrollButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property(nonatomic,strong)NewsListModel *newsListModel;

@property (weak, nonatomic) IBOutlet UIView *parentTitleBarView;

-(void)setCategoryDataInPager:(NSString*)selectedCategory;

@property (strong, nonatomic) IBOutlet UIButton *settingButton;
- (IBAction)settingButtonClickEvent:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *newsTableView;

@property int topScreenFinish;
@property (strong, nonatomic) UnReadNewsClass *unReadNewsClass;
@property (strong, nonatomic) IBOutlet UIView *secondContentView;

-(NewsDao*)getNewsByIndex:(int)index;
-(void)onNotificationClicked:(NSUInteger)allNewsIndex;
-(void)refreshData;
@end
