//
//  HomeScreenViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 08/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "HomeScreenViewControllerAdapters.h"
#import "ApplicationController.h"
#import "NewsDao.h"
#import "Utils.h"
#import "BookMarkClass.h"
#import "UnReadNewsClass.h"
#import "HomeScreenViewController.h"

@interface HomeScreenViewControllerAdapters ()

@end

@implementation HomeScreenViewControllerAdapters

- (void)viewDidLoad {
    [super viewDidLoad];
    [self iniializeInstances];
    [self disableNewsHealineButton];
    [self setDataIntoViews];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self HideShowView];
    [self setBackgroundViews];
    [self getDataFromPreferences];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)iniializeInstances{
    
    bookMarkClass = [[BookMarkClass alloc]init:self];
    localModel = [[[ApplicationController getInstance]getModelFacade]getLocalModel];
    newsListModel = localModel.newsListModel;
    
}


/*
 * Method to set background colour of views
 */
-(void)setBackgroundViews{
    
    self.moreNewsLabel.textColor = [UIColor blueColor];
    [self.newsContentLabel setFont:[UIFont systemFontOfSize:13]];
    [Utils setUpperViewBorder:self.moreNewsView andBorderColour:[UIColor redColor]];
    
}

- (IBAction)newsLinkButtonClickEvent:(id)sender {
    
    [[ApplicationController getInstance]handleEvent:EVENT_ID_NEWS_WEB_VIEW_SCREEN andeventObject:_newsDo];
}

- (IBAction)shareButtonClickEvent:(id)sender {
    
    [Utils takePageScreenShots:self.view andPresentViewController:self];
}

- (IBAction)newsVideoButtonClickedEvent:(id)sender {
    
    [self playVideoInYouTube];
    
}

- (IBAction)newsHeadlineClickEvent:(id)sender {
    bookMarkClass.pageIndex = (int)self.pageIndex;
    
    //On first click of button, set news as bookmark
    if ([sender tag] == 0) {
        [bookMarkClass setBookMarkBackgroundColour];
        [bookMarkClass setBookMarkNews];
        _newsHeadlineButton.tag ++;
        
    }
    
    //On second click of button, remove news from bookmark
    else if ([sender tag] == 1) {
        
        [bookMarkClass setDefaultBackgroundColour];
        [bookMarkClass removeBookMarkNews];
        _newsHeadlineButton.tag --;
        
    }
    
}

/*
 * Method to disable News Headline Button when catergory is Bookmark
 */
-(void)disableNewsHealineButton{
    NSString *currentSelectedCategory =  newsListModel.currentSelectedCategory;
    if ([currentSelectedCategory isEqual:BOOKMARK_CATEGORY_TYPE]) {
        [self.newsHeadlineButton setEnabled:NO];
        [self.newsHeadlineButton setUserInteractionEnabled:NO];
    }
    else{
        [self.newsHeadlineButton setEnabled:YES];
        [self.newsHeadlineButton setUserInteractionEnabled:YES];
    }
    currentSelectedCategory = nil;
}

/*
 * Method to set data into views
 */
-(void)setDataIntoViews{
    
    if (_newsDo!=nil) {
        NSString *authorName = nil;
        NSString *newsDate = nil;
        
        if (_newsDo.imageURL!=nil) {
            self.newsImageView.image = [UIImage imageNamed:_newsDo.imageURL];
        }
        
        if (_newsDo.newsHeading!=nil ) {
            
            _newsHeadlineLabel.text = _newsDo.newsHeading;
        }
        
        if (_newsDo.newsContent!=nil) {
            [self.newsContentLabel setText:_newsDo.newsContent];
        }
        if (_newsDo.authorName!=nil) {
            authorName = [NSString stringWithFormat:@"%@", _newsDo.authorName];
            
        }
        if (_newsDo.newsDay!=nil) {
            newsDate = [NSString stringWithFormat:@"%@",_newsDo.newsDay];
        }
        
        if (authorName!=nil || newsDate!=nil) {
            [self.authorAndPostDayLabel setText:[authorName stringByAppendingFormat:@"/%@",newsDate]];
        }
        if (_newsDo.moreNewsAt!=nil) {
            _moreNewsLinkText.text = _newsDo.moreNewsAt;
        }
        if (_newsDo.videoId!=nil) {
            self.videoID = _newsDo.videoId;
        }
        
        //Mark isReadNews = true i.e it is a read news.
        _newsDo.isReadNews = true;
    }
    [self markAsReadNews];
    
}


/*
 * Method to change text colour and background when Night Mode is enabled
 */
-(void)chanceSettingOnNightModeEnable{
    @autoreleasepool {
        
        self.moreNewsView.backgroundColor = [UIColor blackColor];
        self.moreAtVideoView.backgroundColor = [UIColor blackColor];
        self.moreAtWebView.backgroundColor = [UIColor blackColor];
        self.newsTextBodyView.backgroundColor = [UIColor blackColor];
      
        self.parentView.backgroundColor = [UIColor blackColor];
        
        self.newsHeadlineLabel.textColor = [UIColor whiteColor];
        self.newsContentLabel.textColor = [UIColor whiteColor];
        self.authorAndPostDayLabel.textColor = [UIColor whiteColor];
        self.moreNewsLabel.textColor = [UIColor whiteColor];
        self.videoAtText.textColor = [UIColor whiteColor];
        self.sortByLabel.textColor = [UIColor whiteColor];
        
        self.moreNewsLinkText.textColor = [UIColor blueColor];
        self.youTubeTextLabel.textColor = [UIColor blueColor];
    }
}

/*
 * Method to change text colour and background when Night Mode is disable(reset all colour)
 */
-(void)chanceSettingOnNightModeDisable{
    @autoreleasepool {
        self.moreNewsView.backgroundColor = [UIColor whiteColor];
        self.moreAtVideoView.backgroundColor = [UIColor whiteColor];
        self.moreAtWebView.backgroundColor = [UIColor whiteColor];
        self.newsTextBodyView.backgroundColor = [UIColor whiteColor];
      
        self.parentView.backgroundColor = [UIColor whiteColor];
        
        self.newsContentLabel.textColor = [UIColor grayColor];
        
        self.authorAndPostDayLabel.textColor = [UIColor lightGrayColor];
        
        self.moreNewsLabel.textColor = [UIColor blackColor];
        self.videoAtText.textColor = [UIColor blackColor];
        self.newsHeadlineLabel.textColor = [UIColor blackColor];
        self.sortByLabel.textColor = [UIColor blackColor];
        
        self.moreNewsLinkText.textColor = [UIColor blueColor];
        self.youTubeTextLabel.textColor = [UIColor blueColor];
    }
}

/*
 * Method to get notification, hd image and Night mode enable/Disable state from preferences
 */
-(void)getDataFromPreferences{
    userDefaultPreferences = [UserDefaultPreferences getInstance];
    NSString *nightModeKey = PREF_KEY_NIGHT_MODE;
    
    BOOL switchesState = [userDefaultPreferences getBooleanData:nightModeKey];
    
    //If night mode switch value is true then set selected night mode image
    if (switchesState) {
        [self chanceSettingOnNightModeEnable];
    }
    else{
        [self chanceSettingOnNightModeDisable];
    }
    
    nightModeKey = nil;
    switchesState = nil;
}

/*
 * Method to mark news as ReadNews
 */
-(void)markAsReadNews{
    
    UnReadNewsClass *unReadNewsClass = self.homeScreenViewController.unReadNewsClass;
    
    [unReadNewsClass markAsReadNews:self.newsDo];
    
    
}

/*
 * Method to Play video in YouTube
 */
-(void)playVideoInYouTube{
    if (self.videoID!=nil && ![self.videoID isEqual:@""]) {
        NSString *string = [NSString stringWithFormat:@"http://m.youtube.com/watch?v=%@", self.videoID];
        NSURL *url = [NSURL URLWithString:string];
        UIApplication *app = [UIApplication sharedApplication];
        [app openURL:url];
    }
    
}

/*
 * Method to hide-show view as per satisfied confition
 */
-(void)HideShowView{
    
    if (self.videoID!=nil && ![self.videoID isEqual:@""]) {
        //If video is not nil then show view of youtube video
        [self.moreAtVideoView setHidden:NO];
        [self.moreAtWebView setHidden:YES];
    }
    else{
        //If video is nil then show view news link
        [self.moreAtVideoView setHidden:YES];
        [self.moreAtWebView setHidden:NO];
    }
    
}
@end
