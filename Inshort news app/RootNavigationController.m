//
//  RootNavigationController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 10/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "RootNavigationController.h"


@implementation RootNavigationController
@synthesize rootNavigationController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSBundle mainBundle] loadNibNamed:@"RootNavigationController" owner:self options:nil];
    
//    rootNavigationController = [[UINavigationController alloc] initWithRootViewController:self];
    
    child1NavigationController = [[Child1NavigationController alloc] init];
    
    [self addingBackButtonToNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addingBackButtonToNavigationBar{
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"<Back"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    
}
/*
 * Handled the Back Button Listener.
 */
- (void) handleBack:(id)sender
{
    
    // pop to root view controller
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_ROOT_NAVIGATION_SCREEN];
    
    NSLog(@">>> Back Button Pressed");
    
    
    
}


- (void)didMoveToParentViewController:(UIViewController *)parent
{
    NSLog(@"didMoveToParentViewController");
    if (![parent isEqual:self.parentViewController]) {
        NSLog(@"Back pressed");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)update{
    
    
}

- (IBAction)backButtonPressed:(id)sender {
}

- (IBAction)goToNextScreenButtonClicked:(id)sender {
    
//    [self.navigationController pushViewController:child1NavigationController animated:YES];
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_ROOT_NAVIGATION_CHILD_SCREEN1];
    
}
@end
