//
//  SignInScreenModel.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/17/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "SignInScreenModel.h"
#import "AppConstants.h"
@implementation SignInScreenModel

-(void) initialize{
    [super initialize];
    NSLog(@"Sign In Screen Model : Initialize");
}

/*
 Sends the request to sign in to server.
 */
-(void) signInUser:(NSData*) requestBody{
    [httpRequestHandlerImpl setServiceURL:URL_CHECK_SIGN_IN];
    [httpRequestHandlerImpl setMethodType:HTTP_POST];
    //responseData = [httpRequestHandlerImpl execute:requestBody];
}
@end
