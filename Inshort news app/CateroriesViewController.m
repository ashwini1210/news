//
//  CateroriesViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 14/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "CateroriesViewController.h"
#import "ApplicationController.h"
#import <QuartzCore/QuartzCore.h>

@interface CateroriesViewController ()

@end

@implementation CateroriesViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeInstances];
    [self setScrollViewSize];
    [self initializeViews];
    [self setImagesIntoArray];
    [self highliteSelectedCategory:newsListModel.currentSelectedCategory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Method to initialize layout/views
 */
-(void)initializeViews{
    self.secondParentLayoutView.hidden = YES;
    NSLog(@"Home screen instance %@",self.homeScreenViewController);
}


/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    needToSetScrollViewSize = YES;
    localModel = [[[ApplicationController getInstance]getModelFacade]getLocalModel];
    if (localModel!=nil) {
        newsListModel = localModel.newsListModel;
    }
    
    
}

/*
 * Set scroll view content size
 */
-(void)setScrollViewSize{
    if (needToSetScrollViewSize) {
        //Original length of scroll view
        self.scrollView.contentSize = CGSizeMake(self.firstParentView.frame.size.width, 1320);
    }
    else{
        //Length of scroll view after click on "less" button
        self.scrollView.contentSize = CGSizeMake(self.firstParentView.frame.size.width, 500);
    }
    
}

/*
 * Method to highlite already selected category
 */
-(void)highliteSelectedCategory:(NSString*)selectedCategory{
    @autoreleasepool {
        if (selectedCategory == ALL_NEWS_CATEGORY_TYPE) {
            self.allNewsButton.tag = ALL_NEWS_BUTTON_TAG;
            [self hideSecondView];
            [Utils changeSelectedViewColor:self.allNewsButton andImageView: self.allNewsImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
            
        }
        else if (selectedCategory == TOP_STORIES_CATEGORY_TYPE){
            self.topStoriesButton.tag = TOP_STORIES_BUTTON_TAG;
            [self hideSecondView];
            [Utils changeSelectedViewColor:self.topStoriesButton andImageView: self.topStoriesImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == TRENDING_CATEGORY_TYPE){
            self.trendingButton.tag = TRENDING_BUTTON_TAG;
            [self hideSecondView];
            [Utils changeSelectedViewColor:self.trendingButton andImageView: self.trendingImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
            
        }
        else if (selectedCategory == BOOKMARK_CATEGORY_TYPE){
            self.bookmarkButton.tag = BOOKMARK_BUTTON_TAG;
            [self hideSecondView];
            [Utils changeSelectedViewColor:self.bookmarkButton andImageView: self.bookmarkImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
            
        }
        else if (selectedCategory == UNREAD_CATEGORY_TYPE){
            self.unreadButton.tag = UNREAD_BUTTON_TAG;
            [self hideSecondView];
            [Utils changeSelectedViewColor:self.unreadButton andImageView: self.unreadImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
            
        }
        else if (selectedCategory == INDIA_CATEGORY_TYPE){
            self.indiaButton.tag = INDIA_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.indiaButton andImageView: self.indiaImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == BUISNESS_CATEGORY_TYPE){
            self.buisnessButton.tag = BUISNESS_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.buisnessButton andImageView: self.buisnessImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == POLITICS_CATEGORY_TYPE){
            self.politicsButton.tag = POLITICS_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.politicsButton andImageView: self.politicsImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == SPORTS_CATEGORY_TYPE){
            self.sportsButton.tag = SPORTS_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.sportsButton andImageView: self.sportsImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == TECHNOLOGY_CATEGORY_TYPE){
            self.technologyButton.tag = TECHNOLOGY_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.technologyButton andImageView: self.technologyImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == STARTUP_CATEGORY_TYPE){
            self.startUpButton.tag = STARTUP_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.startUpButton andImageView: self.startUpImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == ENTERTAINMENT_CATEGORY_TYPE ){
            self.entertainmentButton.tag = ENTERTAINMENT_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.entertainmentButton andImageView: self.entertainmentImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == INTERNATIONAL_CATEGORY_TYPE){
            self.internationalButton.tag = INTERNATIONAL_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.internationalButton andImageView: self.internationalImageViews unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
        else if (selectedCategory == HEALTH_CATEGORY_TYPE){
            self.healthButton.tag = HEALTH_BUTTON_TAG;
            [self showSecondView];
            [Utils changeSelectedViewColor:self.healthButton andImageView: self.healthImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
        }
    }
}

/*
 * Method to set animation to view
 */
-(void)setAnimationToView:(UIView*)animatingView{
    CATransition *animation = [CATransition animation];
    
    //Animation while click on more button
    if (self.secondParentLayoutView.isHidden) {
        animation.type = kCATransitionFromTop;
        animation.duration = 0.4;
        [animatingView.layer addAnimation:animation forKey:nil];
    }
    else{
        //Animation while click on less button
        self.moreNewsView.hidden = YES;
        animation.type = kCATransitionFromBottom;
        animation.duration = 0.4;
        [animatingView.layer addAnimation:animation forKey:nil];
    }
    
}



/*
 * Method to set images into array
 */
-(void)setImagesIntoArray{
    selectedImagesArray = @[@"all_news_selected.png",@"top_stories_selsecte.png",@"trending_selected.png",@"bookmark_selecte.png",@"unread_selected.png",@"",@"india_selected.png",@"buisness_selected.png",@"politics_selected.png",@"sports_selected.png",@"technology_selected",@"startup_selected.png",@"entertainment_selected.png",@"international_selected.png",@"health_selected.png",@""];
    
    unSelectedImagesArray = @[@"all_news.png",@"top_stories.png",@"trending.png",@"bookmark.png",@"unread.png",@"more.png",@"india.png",@"buisness.png",@"politics.png",@"sports.png",@"technology.png",@"startup.png",@"entertainment.png",@"intertational.png",@"health.png",@"less.png"];
}


- (IBAction)backButtonClickEvent:(id)sender {
    
    [self finishScreen];
    
}

- (IBAction)allNewsButtonClcikEvent:(id)sender {
    [self highliteSelectedCategory:ALL_NEWS_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:ALL_NEWS_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)topStoriesButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:TOP_STORIES_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:TOP_STORIES_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)trendingButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:TRENDING_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:TRENDING_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)bookmarkButtonClickEvent:(id)sender {
    self.bookmarkButton.tag = BOOKMARK_BUTTON_TAG;
    [Utils changeSelectedViewColor:self.bookmarkButton andImageView: self.bookmarkImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    [self.homeScreenViewController setCategoryDataInPager:BOOKMARK_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)unreadButtonClickEvent:(id)sender {
    self.unreadButton.tag = UNREAD_BUTTON_TAG;
    [Utils changeSelectedViewColor:self.unreadButton andImageView: self.unreadImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    [self.homeScreenViewController setCategoryDataInPager:UNREAD_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)moreButtonClickEvent:(id)sender {
    self.moreImageView.tag = MORE_BUTTON_TAG;
    [self showSecondView];
}

- (IBAction)indiaButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:INDIA_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:INDIA_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)buisnessButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:BUISNESS_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:BUISNESS_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)politicsButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:POLITICS_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:POLITICS_CATEGORY_TYPE];
    [self finishScreen];
}


- (IBAction)sportsButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:SPORTS_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:SPORTS_CATEGORY_TYPE];
    [self finishScreen];
}


- (IBAction)technologyButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:TECHNOLOGY_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:TECHNOLOGY_CATEGORY_TYPE];
    [self finishScreen];
    
}

- (IBAction)startUpButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:STARTUP_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:STARTUP_CATEGORY_TYPE];
    [self finishScreen];
    
}

- (IBAction)entertainmentButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:ENTERTAINMENT_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:ENTERTAINMENT_CATEGORY_TYPE];
    [self finishScreen];
}

- (IBAction)internationalButtonClickButton:(id)sender {
    [self highliteSelectedCategory:INTERNATIONAL_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:INTERNATIONAL_CATEGORY_TYPE];
    [self finishScreen];
    
}

- (IBAction)healthButtonClickEvent:(id)sender {
    [self highliteSelectedCategory:HEALTH_CATEGORY_TYPE];
    [self.homeScreenViewController setCategoryDataInPager:HEALTH_CATEGORY_TYPE];
    
    [self finishScreen];
    
}

- (IBAction)lessButtonClickEvent:(id)sender {
    self.lessButton.tag = LESS_BUTTON_TAG;
    [self hideSecondView];
}

/*
 * Method to hide view which contain layout below more button
 */
-(void)hideSecondView{
    [self setAnimationToView:self.secondParentLayoutView];
    [self setViewBooleanValue];
    [self setScrollViewSize];
}

/*
 * Method to show view which contain layout below more button
 */
-(void)showSecondView{
    [self setAnimationToView:self.secondParentLayoutView];
    self.secondParentLayoutView.hidden = NO;
    needToSetScrollViewSize = YES;
    [self setScrollViewSize];
}

-(void)finishScreen{
    [[ApplicationController getInstance]handleEvent:EVENT_ID_FINISH_SCREEN];
}
/*
 * Call back of CustomAlertViewDelegate
 */




-(void)onScreenPopedUp{
    NSLog(@"On screen poped up");
    
}

-(void)onTopScreenFinished{
    NSLog(@"On top screen finished");
    //    [self finishScreen];
}

/*
 * Method to set boolean value of views accourding to display and hide.
 */
-(void)setViewBooleanValue{
    self.secondParentLayoutView.hidden = YES;
    needToSetScrollViewSize = NO;
    self.moreNewsView.hidden = NO;
}

-(void)destroy{
    NSLog(@"Destroy of categories screen");
    self.previousSelectedButton = nil;
    self.allNewsImageView = nil;
    self.topStoriesImageView =nil;
    self.trendingImageView= nil;
    self.bookmarkImageView= nil;
    self.unreadImageView= nil;
    self.moreImageView= nil;
    self.indiaImageView= nil;
    self.buisnessImageView= nil;
    self.politicsImageView= nil;
    self.sportsImageView= nil;
    self.technologyImageView= nil;
    self.startUpImageView= nil;
    self.entertainmentImageView= nil;
    self.internationalImageViews= nil;
    self.healthImageView= nil;
    self.lessImageView= nil;
    self.scrollView= nil;
    self.allNewsView= nil;
    self.topStoriesView= nil;
    self.trendingNewsView= nil;
    self.bookMarkNewsView= nil;
    self.unreadNewsView= nil;
    self.moreNewsView= nil;
    self.secondParentLayoutView= nil;
    self.indianNewsView= nil;
    self.previousSelectedButton= nil;
    self.previousSelectedImageView= nil;
    self.backButton= nil;
    self.allNewsButton= nil;
    self.topStoriesButton= nil;
    self.trendingButton= nil;
    self.bookmarkButton= nil;
    self.unreadButton= nil;
    self.indiaButton= nil;
    self.moreButton= nil;
    self.buisnessButton= nil;
    self.politicsButton= nil;
    self.sportsButton= nil;
    self.technologyButton= nil;
    self.startUpButton= nil;
    self.entertainmentButton= nil;
    self.internationalButton= nil;
    self.healthButton= nil;
    self.lessButton= nil;
    localModel= nil;
    newsListModel= nil;
    
    
}
@end
