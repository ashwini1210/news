//
//  CustomPageViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 13/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "ApplicationController.h"
#import "ChildPageViewController.h"

@interface CustomPageViewController : UIPageViewController<UIPageViewControllerDataSource, AbstractViewController>
- (IBAction)goToNextScreenButtonPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

@end
