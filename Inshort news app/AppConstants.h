//
//  AppConstants.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#ifndef __APP_CONSTANTS_H__
#define __APP_CONSTANTS_H__

#import <Foundation/Foundation.h>

// start View Factory Constant here

typedef enum ViewFactoryScreen
{
    //Sample ViewFactoryScreen
    VF_ROOT_VIEW_SCREEN,
    
    VF_MAIN_SCREEN,
    VF_TEST_SCREEN,
    VF_ROOT_NAVIGATION_SCREEN,
    VF_ROOT_NAVIGATION_CHILD_SCREEN1,
    VF_ROOT_NAVIGATION_CHILD_SCREEN2,
    VF_TEST1_VIEW_CONTROLLER,
    VF_CUSTOM_TABLE_VIEW_SCREEN,
    VF_CUSTOM_PAGE_VIEW_SCREEN,
    VF_CUSTOM_COLLECTION_VIEW_SCREEN,
    
    //Actual App ViewFactoryScreen
    VF_SPLASH_SCREEN,
    VF_HOME_SCREEN,
    VF_NEWS_WEB_VIEW_SCREEN,
    VF_CATEGORIES_VIEW_SCREEN,
    VF_SETTING_SCREEN,
    VF_DETAIL_NEWS_PAGER_SCREEN,
    
    
} VF;


typedef enum Events
{
    //Sample Events ID'S
    EVENT_ID_FINISH_SCREEN,
    
    EVENT_ID_MAIN_SCREEN,
    EVENT_ID_TEST_SCREEN,
    EVENT_ID_ROOT_NAVIGATION_SCREEN,
    EVENT_ID_ROOT_NAVIGATION_CHILD_SCREEN1,
    EVENT_ID_ROOT_NAVIGATION_CHILD_SCREEN2,
    EVENT_ID_TEST1_VIEW_CONTROLLER,
    EVENT_ID_FINISH_ROOT_NAVIGATION_SCREEN,
    EVENT_ID_FINISH_CHILD_NAVIGATION_SCREEN,
    EVENT_ID_ON_SCREEN_FINISH,
    EVENT_ID_CUSTOM_TABLE_VIEW_SCREEN,
    EVENT_ID_CUSTOM_PAGE_VIEW_SCREEN,
    EVENT_ID_CUSTOM_COLLECTION_VIEW_SCREEN,
    
    
    //Actual App Events ID'S
    EVENT_ID_SPLASH_SCREEN,
    EVENT_ID_HOME_SCREEN,
    EVENT_ID_NEWS_WEB_VIEW_SCREEN,
    EVENT_ID_CATEGORIES_VIEW_SCREEN,
    EVENT_ID_SETTING_SCREEN,
    EVENT_ID_FINISH_TOP_SCREENS,
    EVENT_ID_DETAIL_NEWS_PAGER_SCREEN,
    
    
}EventId;

typedef enum ViewControllerType
{
    VCT_VIEW_CONTROLLER,
    VCT_ROOT_NAVIGATION_CONTROLLER,
    VCT_CHILD_NAVIGATION_CONTROLLER,
    
}VCT;
// end view factory constatn here

extern NSString* const SERVER_URL;
extern NSString* const URL_CHECK_SIGN_IN;
extern NSString* const URL_CHECK_SIGN_UP;
extern NSString* const URL_CHECK_VERIFICATION;
extern NSString* const URL_CHECK_GET_ALL_USERS;
extern NSString* const CUSTOM_ALERT_ERROR_TITLE;
extern NSString* const CUSTOM_ALERT_COMMING_SOON_MESSAGE;
extern NSString* const CUSTOM_ALERT_CANCEL_BUTTON_TEXT;
extern NSString* const CUSTOM_ALERT_YES_BUTTON_TEXT;
extern NSString* const CUSTOM_ALERT_NO_BUTTON_TEXT;
extern NSString* const CUSTOM_ALERT_OK_BUTTON_TEXT ;
extern NSString* const CUSTOM_ALERT_SUCCESS_MESSAGE;
extern NSString* const PLEASE_WAIT;
extern NSString* const ALL_NEWS_CATEGORY_TYPE;
extern NSString* const TOP_STORIES_CATEGORY_TYPE;
extern NSString* const TRENDING_CATEGORY_TYPE;
extern NSString* const BOOKMARK_CATEGORY_TYPE;
extern NSString* const UNREAD_CATEGORY_TYPE;
extern NSString* const MORE_CATEGORY_TYPE;
extern NSString* const INDIA_CATEGORY_TYPE;
extern NSString* const BUISNESS_CATEGORY_TYPE;
extern NSString* const POLITICS_CATEGORY_TYPE;
extern NSString* const SPORTS_CATEGORY_TYPE;
extern NSString* const TECHNOLOGY_CATEGORY_TYPE;
extern NSString* const STARTUP_CATEGORY_TYPE;
extern NSString* const ENTERTAINMENT_CATEGORY_TYPE;
extern NSString* const INTERNATIONAL_CATEGORY_TYPE;
extern NSString* const HEALTH_CATEGORY_TYPE;
extern NSString* const LESS_CATEGORY_TYPE;


extern int const SPLASH_DISPLAY_LENGTH;
extern int const SUCCESS_CODE;
extern int const ALL_NEWS_BUTTON_TAG;
extern int const TOP_STORIES_BUTTON_TAG;
extern int const TRENDING_BUTTON_TAG;
extern int const BOOKMARK_BUTTON_TAG;
extern int const UNREAD_BUTTON_TAG;
extern int const MORE_BUTTON_TAG;
extern int const INDIA_BUTTON_TAG;
extern int const BUISNESS_BUTTON_TAG;
extern int const POLITICS_BUTTON_TAG;
extern int const SPORTS_BUTTON_TAG;
extern int const TECHNOLOGY_BUTTON_TAG;
extern int const STARTUP_BUTTON_TAG;
extern int const ENTERTAINMENT_BUTTON_TAG;
extern int const INTERNATIONAL_BUTTON_TAG;
extern int const HEALTH_BUTTON_TAG;
extern int const LESS_BUTTON_TAG;
extern int const NOTIFICATION_BUTTON_TAG;
extern int const HD_IMAGE_BUTTON_TAG;
extern int const NIGHT_MODE_BUTTON_TAG;
extern int const LIST_MODE_BUTTON_TAG;
extern int const SHARE_APP_BUTTON_TAG;
extern int const RATE_APP_BUTTON_TAG;
extern int const FEEDBACK_BUTTON_TAG;
extern int const TERM_CONDITION_BUTTON_TAG;
extern int const PRIVACY_BUTTON_TAG;


/*FEEDBACK SCREEN VIEW CONTROLLER CONSTANTS*/
extern NSString *const FEEDBACK_SCREEN_MAIL_TITLE;
extern NSString *const FEEDBACK_SCREEN_MAIL_SAVED;
extern NSString *const FEEDBACK_SCREEN_MAIL_FAILED;
extern NSString *const FEEDBACK_SCREEN_MAIL_SENT ;
extern NSString *const FEEDBACK_SCREEN_MAIL_SAVED_MESSAGE;
extern NSString *const FEEDBACK_SCREEN_MAIL_FAILED_MESSAGE;
extern NSString *const FEEDBACK_SCREEN_MAIL_SENT_MESSAGE;


/*SETTING SCREEN VIEW CONTROLLER CONSTANTS*/
extern NSString *const SETTING_SCREEN_COMING_SOON_MESSAGE;

/*DETAIL LIST PAGER VIEW CONTROLLER */
extern NSString *const SELECTED_CELL_INDEX_KEY ;
extern NSString *const SELECTED_CATEGORY_ARRAY;

/*HOME SCREEN PAGER VIEW CONTROLLER */
extern NSString *const NO_SELECTED_CATEGORY;
extern NSString *const NO_SELECTED_BOOKMARK;

/*NOTIFICATION KEYS*/
extern NSString *const NOTIFICATION_NEWS_KEY ;
extern NSString *const NOTIFICATION_KEY_NEWS_ID;
extern NSString *const NOTIFICATION_KEY_NEWS_HEADING;
extern NSString *const  NOTIFICATION_KEY_NEWS_DAO;



#define APP_THEME_LIGHT_ORANGE_COLOR @"FF9B94"

@interface AppConstants : NSObject

@end

#endif