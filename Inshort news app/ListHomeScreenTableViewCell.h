//
//  ListHomeScreenTableViewCell.h
//  IosApplicationFrameworkProject
//
//  Created by test on 01/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDefaultPreferences.h"


@interface ListHomeScreenTableViewCell : UITableViewCell
{
    UserDefaultPreferences *userDefaultPreferences;
}
@property (strong, nonatomic) IBOutlet UIImageView *newsImageView;
@property (strong, nonatomic) IBOutlet UITextView *newsHeadlinesLabel;

@property (strong, nonatomic) IBOutlet UILabel *newsDateLabel;
@property (strong, nonatomic) IBOutlet UIView *contentViews;


@end
