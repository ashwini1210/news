//
//  RootUIViewController.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/4/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "RootUIViewController.h"
#import "ApplicationController.h"


@interface RootUIViewController ()

@end

@implementation RootUIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[ApplicationController getInstance] handleEvent:EVENT_ID_SPLASH_SCREEN];
     //[[ApplicationController getInstance] handleEvent:EVENT_ID_HOME_SCREEN];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/**
 * To initialize the Views or Screens, Models
 *
 */
-(void) initialize{
    NSLog(@"Root Controller initialize");
}


/**
 * To reinitialize the View or Screen. This is for some cases when user
 * implement freeResource to optimize memory issue.
 */


-(void) reInitialize{
    NSLog(@"Root Controller reInitialize");
    
}

/**
 * To free only View releated stuff.
 *
 */
-(void) freeResources{
    NSLog(@"Root Controller freeResources");
    
}

/**
 * Destroy View and Model Data for the given Screen.
 *
 */
-(void) destory{
    NSLog(@"Root Controller destory");
    
}

/**
 * This function get called whenever AbstractView redisplay on device screen
 *
 */

-(void) enable{
    NSLog(@"Root Controller enable");
    
}


/**
 * This function get called whenever AbstractView overlapped by another
 * AbstractView
 */

-(void) disbale{
    NSLog(@"Root Controller disbale");
    
}



/**
 * To notify the AbstarctView, that there is an interruption. So
 * AbstractView can handle all the required condition before going
 * application in background.
 *
 */

-(void) showNotify{
    NSLog(@"Root Controller showNotify");
    
}

/**
 * To notify the AbstractView that application will be visible on the screen
 * after calling this method.
 */

-(void) hideNotify{
    NSLog(@"Root Controller hideNotify");
}


/**
 * To update a view
 */

-(void) update{
    NSLog(@"Root Controller update");
    
}


@end
