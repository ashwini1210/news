//
//  Test1ViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 10/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "ApplicationController.h"
@interface Test1ViewController : UIViewController<AbstractViewController>
- (IBAction)goToNextScreenButtonClicked:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

@end
