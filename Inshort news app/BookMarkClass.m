//
//  BookMarkClass.m
//  IosApplicationFrameworkProject
//
//  Created by test on 05/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "BookMarkClass.h"
#import "NewsDao.h"

@implementation BookMarkClass

/*
 * Initialize Method
 */
-(id)init:(HomeScreenViewControllerAdapters*)homeScreenAdapters{
    self = [super init];
    homeScreenViewControllerAdapters = homeScreenAdapters;
    [self initialize];
    return self;
}

/*
 * Method to initialize instances
 */
-(void)initialize{
    
    localModel = [[[ApplicationController getInstance] getModelFacade] getLocalModel];
    newsListModel = localModel.newsListModel;
    
}

/*
 * Method to set background colour of views when news is bookmark
 */
-(void)setBookMarkBackgroundColour{
    homeScreenViewControllerAdapters.newsHeadlineLabel.textColor = [UIColor redColor];
}

/*
 * Method to set background colour of views when news is remove from bookmark
 */
-(void)setDefaultBackgroundColour{
    homeScreenViewControllerAdapters.newsHeadlineLabel.textColor = [UIColor blackColor];
}

/*
 * Method to set news as bookmark
 */
-(void)setBookMarkNews{
    
    [self setBookMarkNews:newsListModel.currentSelectedCategory];
}

/*
 * Method to set news as bookmark as per category
 */
-(void)setBookMarkNews:(NSString*)selectedCategory{
    
    NSMutableArray *bookmarkArray = newsListModel.bookMarkArray;
    if (bookmarkArray!=nil) {
        NewsDao *newsDao = [Utils getDaoFromCategory:selectedCategory andNewsIndex:self.pageIndex];
        
        //If clicked newsDao is not present in bookmark array then only add it into array.
        if (![bookmarkArray containsObject:newsDao]) {
            [bookmarkArray addObject:newsDao];
        }
        
        newsDao = nil;
        
    }
   
}

/*
 * Method to remove news from bookmark array
 */
-(void)removeBookMarkNews{
    
    [self removeBookMarkNews:newsListModel.currentSelectedCategory];
    
}

/*
 * Method to remove news from bookmark array as per selected category
 */
-(void)removeBookMarkNews:(NSString*)selectedCategory{
    @autoreleasepool {
        
        NewsDao *newsDao = [Utils getDaoFromCategory:selectedCategory andNewsIndex:self.pageIndex];
        NSInteger newsID = newsDao.newsId;
        NSMutableArray *bookmarkArray = newsListModel.bookMarkArray;
        int bookMarkArrayCount = [bookmarkArray count];
        
        for (int i = 0; i<bookMarkArrayCount; i++) {
            NewsDao *tempNewsDao = [bookmarkArray objectAtIndex:i];
            NSInteger tempNewsID = tempNewsDao.newsId;
            if (tempNewsID == newsID) {
                int index = [bookmarkArray indexOfObject:tempNewsDao];
                [bookmarkArray removeObjectAtIndex:index];
            }
            tempNewsDao = nil;
            
        }
        
        newsDao = nil;
        bookmarkArray = nil;
    }
    
}


@end
