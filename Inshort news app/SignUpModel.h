//
//  SignUpModel.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/17/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "BaseModel.h"

@interface SignUpModel : BaseModel
{
//    {"username":"ashish","password":"123456","email":"ashish@syslogic.in","mobile_number":"+919860677800"}
    /*
     * Username of the user.
     */
    NSString *userName;
    /*
     * Password of the user.
     */
    NSString *password;
    /*
     * emailAdd of the user.
     */
    NSString *emailAdd;
    /*
     * Mobile number of the user.
     */
    long mobNum;
}
-(void)setUserName:(NSString*)userName;
-(void)setPassword:(NSString*)password;
-(void)setEmailAdd:(NSString*)emailId;
-(void)setMob:(long)mobileNum;

@property(nonatomic,strong)NSString *userName;
@property(nonatomic,strong)NSString *password;
@property(nonatomic,strong)NSString *emailAdd;

@end
