//
//  RootUIViewController.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/4/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "AbstractViewController.h"
#import <UIKit/UIKit.h>
@interface RootUIViewController : UIViewController <AbstractViewController>

@end
