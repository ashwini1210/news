//
//  UserDefaultPreferences.h
//  UserDefaultSampleProject
//
//  Created by Ranjit singh on 8/31/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * Key for NOTIFICATION
 */
#define PREF_KEY_NOTIFICATION @"pref_notification_data";

/*
 * Key for hd image
 */
#define PREF_KEY_HD_IMAGE   @"pref_hd_image_data";

/*
 * Key for night mode
 */
#define PREF_KEY_NIGHT_MODE   @"pref_night_mode_data";

/*
 * Key for list mode
 */
#define PREF_KEY_LIST_MODE   @"pref_list_mode_data";


@interface UserDefaultPreferences : NSObject


@property (nonatomic,retain) NSUserDefaults *preferences;

+(UserDefaultPreferences *)getInstance;

-(void) saveString:(NSString*) key stringValue:(NSString*)value;
-(NSString*) getString:(NSString*) key;

-(void) saveNSData:(NSString*) key stringValue:(NSData*)value;
-(NSData*) getNSData:(NSString*) key;


-(void)setBool:(BOOL)value forKey:(NSString*) key;
-(BOOL) getBooleanData:(NSString*) key;




@end
