//
//  SplashScreenViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 18/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "ApplicationController.h"
#import "AppConstants.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setSplashImage];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self setTimerToLaunchNextScreen];
}


/*
 * Method to set splash image to imageview.
 */
-(void)setSplashImage{
    
    @autoreleasepool {
        
        //Here we set the filepath of splash screen image.
        NSString *fileLocation = [[NSBundle mainBundle] pathForResource:@"iphone_splash" ofType:@"png"];
        
        UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation];
        
        //Set splash image to splashBackgroundImageview.
        [self.splashBackgroundImageView setImage:yourImage];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Method to set Timer on Splash Screen to launch next screen (Login Screen).
 */
-(void) setTimerToLaunchNextScreen{
    
    [NSTimer scheduledTimerWithTimeInterval:SPLASH_DISPLAY_LENGTH
                                     target:self
                                   selector:@selector(launchNextScreen)
                                   userInfo:nil
                                    repeats:NO];
    
    
}



/*
 * NSTimer Selector Method to launch Next Screen.
 */
-(void)launchNextScreen{
    
     [[ApplicationController getInstance] handleEvent:EVENT_ID_HOME_SCREEN];
   }


-(void) update{
    
    
}

-(void)onScreenPopedUp{
   
}

/*
 * Gets callback on top screen finished
 */
-(void) onTopScreenFinished{
   
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [self destroy];
}

/**
 * Destroy View and Model Data for the given Screen.
 *
 */
-(void) destroy{
    self.splashBackgroundImageView = nil;
}



@end
