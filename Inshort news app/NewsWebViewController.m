//
//  NewsWebViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 11/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "NewsWebViewController.h"
#import "ApplicationController.h"

@interface NewsWebViewController ()

@end

@implementation NewsWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initializeViews];
    [self loadWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 * Method to initialize instances
 */
-(void)initializeViews{
    customProgressDialog = [CustomAlertView createCustomProgressDialog:PLEASE_WAIT delegate:self identifier:1];
    errorCustomAlertView = [CustomAlertView createCustomAlertView:CUSTOM_ALERT_ERROR_TITLE alertMessage:@"" leftButtonTitle:CUSTOM_ALERT_OK_BUTTON_TEXT rightButtonTitle:nil delegate:self identifier:2];
    
}


/*
 * Method to load web view
 */
-(void)loadWebView{
    //Dummy Implementation
    //    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"new_bbc_homepage" ofType:@"html"];
    //    NSString* htmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    //    [self.newsWebView loadHTMLString:htmlString baseURL:nil];
    
    
    //Actual Implementation after API's integration
    NSString *urlString = _newsDo.descriptiveNewsLink;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.newsWebView loadRequest:urlRequest];
    [self.view addSubview:self.newsWebView];
    
}

- (IBAction)backButtonClickEvent:(id)sender {
    [[ApplicationController getInstance]handleEvent:EVENT_ID_FINISH_SCREEN];
}

-(void)destroy{
    customProgressDialog =nil;
    errorCustomAlertView=nil;
    self.newsWebView=nil;
    self.backButton=nil;
}
@end
