//
//  AppDelegate.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "AppDelegate.h"
#import "RootUIViewController.h"
#import "ApplicationController.h"



@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
    //here set the our root view controller to the winddow's root.
    RootUIViewController *rootUIViewController =[RootUIViewController alloc];
    
    self.window = [[UIWindow alloc]
                   initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = rootUIViewController;
    
    //below function should get call only once in the whole life cycle of the app.
    [[ApplicationController getInstance] initialize];
    
    [[[ApplicationController getInstance]getUiConroller]setRootViewController:self.window.rootViewController];
    
    //Register Local Notification
    [self registerForRemoteNotification:application];
    
    //Initialize Notification Class
    [self initializeNotification];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
   
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
   
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  
    [notificationClass initialize];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
 * Method to register Notification
 */
- (void)registerForRemoteNotification:(UIApplication*)application{
    
    
    // are we running on iOS8?
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeAlert|UIUserNotificationTypeSound) categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else // iOS 7 or earlier
    {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    // This is in order to allow views that do not support landscape mode to be able to load in
    
    return UIInterfaceOrientationMaskPortrait;
}

/*
 * Method to initialize Notification class
 */
-(void)initializeNotification{
    LocalModel *localModel = [[[ApplicationController getInstance]getModelFacade]getLocalModel];
    
    if (localModel!=nil) {
        notificationClass = localModel.notificationClass;
        
        if (notificationClass==nil) {
            notificationClass = [[NotificationClass alloc]init];
            localModel.notificationClass = notificationClass;
        }
    }
    
    
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
   
    NSLog(@"In didReceiveLocalNotification method of AppDeligate");
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    NSLog(@"Application state %d",state);
    if (state != UIApplicationStateActive)
    {
         [self openNotificationNewsPage:notification];
    }
   
   
}

-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler{
     NSLog(@"In handleActionWithIdentifier method of AppDeligate");
   
}

/*
 * Method to open news whose heading is present in Notification
 */
-(void)openNotificationNewsPage:(UILocalNotification *)notification {
   
    if (notification)
    {
         [notificationClass switchToLocalNotificationNews:notification];
    }
}
@end
