//
//  AppDelegate.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationClass.h"
#import "LocalModel.h"
#import "ApplicationController.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NotificationClass *notificationClass;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) UIViewController *splashscreenViewController;


@end

