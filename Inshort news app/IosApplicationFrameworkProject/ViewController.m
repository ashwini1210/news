//
//  ViewController.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "ViewController.h"
#import "ApplicationController.h"


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.76f green:0.81f blue:0.87f alpha:1];
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame = CGRectMake(20, 20, 200, 44); // position in the parent view and set the size of the button
    [myButton setTitle:@"Click Me!" forState:UIControlStateNormal];
    
    [self.view addSubview:myButton];
    
    
    NSLog(@"Show self.view:%@",self.view);
//    [[ApplicationController getInstance] setMainView:self.view];
//    
//    [[ApplicationController getInstance] handleEvent:VF_SPLASH_SCREEN];

    NSLog(@"Framework is running properly");
    
   /* UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert View" message:@"This is alter view" delegate:nil cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    
    [alertView show];*/
}
       // Do any additional setup after loading the view, typically from a nib.

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressButton:(id)sender {
    
    NSLog(@"you pressed the button");
}
@end
