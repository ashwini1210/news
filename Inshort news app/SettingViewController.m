//
//  SettingViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 18/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "SettingViewController.h"
#import "ApplicationController.h"
#import "RateAppClass.h"
#import "FeedbackAppClass.h"


const NSInteger DEFAULT_SWITCH_TAG = 100;
const NSInteger CUSTOM_ALERT_VIEW_WITH_THREE_BUTTON_IDENTIFIER = 1;
const NSInteger CUSTOM_ALERT_VIEW_WITH_TWO_BUTTON_IDENTIFIER = 2;

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeInstances];
    [self setScrollViewSize];
    [self getDataFromPreferences];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
}

/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    localModel = [[[ApplicationController getInstance]getModelFacade]getLocalModel];
    
    self.customAlertViewWithThreeButtons = [CustomAlertView createCustomAlertView:@"Please rate \"Pollyannaish\"!" alertMessage:@"If you like it we'd like to know." leftButtonTitle:@"Rate It Now" centerButtonTitle:@"Remind Me Later" rightButtonTitle:@"No, Thanks" delegate:self identifier:CUSTOM_ALERT_VIEW_WITH_THREE_BUTTON_IDENTIFIER];
    
    self.customAlertView = [CustomAlertView createCustomAlertView:@"" alertMessage:@"" leftButtonTitle:CUSTOM_ALERT_OK_BUTTON_TEXT rightButtonTitle:nil delegate:self identifier:CUSTOM_ALERT_VIEW_WITH_TWO_BUTTON_IDENTIFIER];
    
    notificationClass = localModel.notificationClass;
    
    userDefaultPreferences = [UserDefaultPreferences getInstance];
    self.mFMailComposeViewControllerDelegate = self;
    [self setImagesIntoArray];
}

/*
 * Method to set images into array
 */
-(void)setImagesIntoArray{
    selectedImagesArray = @[@"notification_sel.png",@"hd_image_sel.png",@"night_mode_sel.png",@"list_mode_sel.png",@"invite_frnd_sel.png",@"rate_app_sel.png",@"feedback_app_sel.png",@"term_condition_sel.png",@"privacy_sel.png"];
    
    unSelectedImagesArray = @[@"notification.png",@"hd_image.png",@"night_mode.png",@"list_mode.png",@"invite_frnd.png",@"rate_app.png",@"feedback_app.png",@"term_condition.png",@"privacy.png"];
}

/*
 * Set scroll view content size
 */
-(void)setScrollViewSize{
    //Original length of scroll view
    self.SCROLLVIEW.contentSize = CGSizeMake(320, 1300);
}

- (IBAction)notificationSwitch:(id)sender {
    
    self.notificationSwitch.tag = NOTIFICATION_BUTTON_TAG;
    
    if (self.notificationSwitch.on) {
        self.notificationImageView.image = [UIImage imageNamed:selectedImagesArray[self.notificationSwitch.tag]];
        
    }
    else{
        self.notificationImageView.image = [UIImage imageNamed:unSelectedImagesArray[self.notificationSwitch.tag]];
        
    }
    [notificationClass checkNotificationSwitchState];
    [self storeDataIntoPreferences:NOTIFICATION_BUTTON_TAG];
}



- (IBAction)hdImageSwitch:(id)sender {
    
    self.notificationSwitch.tag = HD_IMAGE_BUTTON_TAG;
    
    if (self.hdImageSwitch.on) {
        self.hdImageView.image = [UIImage imageNamed:selectedImagesArray[self.notificationSwitch.tag]];
    }
    else{
        self.hdImageView.image = [UIImage imageNamed:unSelectedImagesArray[self.notificationSwitch.tag]];
    }
    
    [self storeDataIntoPreferences:HD_IMAGE_BUTTON_TAG];
}



- (IBAction)nightModeSwitch:(id)sender {
    
    self.nightModeSwitch.tag = NIGHT_MODE_BUTTON_TAG;
    
    if (self.nightModeSwitch.on) {
        self.nightModeImageView.image = [UIImage imageNamed:selectedImagesArray[self.nightModeSwitch.tag]];
    }
    else{
        self.nightModeImageView.image = [UIImage imageNamed:unSelectedImagesArray[self.nightModeSwitch.tag]];
    }
    
    [self storeDataIntoPreferences:NIGHT_MODE_BUTTON_TAG];
}


- (IBAction)listModeSwitch:(id)sender {
    
    self.listModeSwitch.tag = LIST_MODE_BUTTON_TAG;
    
    if (self.listModeSwitch.on) {
        self.listModeImageView.image = [UIImage imageNamed:selectedImagesArray[self.listModeSwitch.tag]];
        
        
    }
    else{
        self.listModeImageView.image = [UIImage imageNamed:unSelectedImagesArray[self.listModeSwitch.tag]];
    }
    
    [self storeDataIntoPreferences:LIST_MODE_BUTTON_TAG];
}

- (IBAction)shareAppButtonClickEvent:(id)sender {
    self.shareAppButton.tag = SHARE_APP_BUTTON_TAG;
    [Utils changeSelectedViewColor: self.shareAppButton andImageView:self.shareAppImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    
    [self shareData];
    
}


- (IBAction)rateAppButtonClickEvent:(id)sender {
    self.rateAppButton.tag = RATE_APP_BUTTON_TAG;
    [Utils changeSelectedViewColor: self.rateAppButton andImageView:self.rateAppImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    self.rateAppClass = [[RateAppClass alloc]init];
    [self.rateAppClass initialize:self];
    
}

- (IBAction)feedBackButtonClickEvent:(id)sender {
    self.feedBackButton.tag = FEEDBACK_BUTTON_TAG;
    [Utils changeSelectedViewColor: self.feedBackButton andImageView:self.feedBackImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    
    self.feedbackAppClass = [[FeedbackAppClass alloc]init];
    [self.feedbackAppClass initialize:self];
}

- (IBAction)termConditionButtonClickEvent:(id)sender {
    self.termConditionButton.tag = TERM_CONDITION_BUTTON_TAG;
    [Utils changeSelectedViewColor: self.termConditionButton andImageView:self.termConditionImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    [self.customAlertView setMessage:[self setSmilyMessage]];
    [self.customAlertView show:self];
    
}
- (IBAction)privacyButtonClickEvent:(id)sender {
    self.privacyButton.tag = PRIVACY_BUTTON_TAG;
    [Utils changeSelectedViewColor: self.privacyButton andImageView:self.privacyImageView unSelectedImagesArray:unSelectedImagesArray selectedImagesArray:selectedImagesArray];
    [self.customAlertView setMessage:[self setSmilyMessage]];
    [self.customAlertView show:self];
    
}

- (IBAction)returnButtonClickEvent:(id)sender {
    self.homeScreenViewController.topScreenFinish = VF_SETTING_SCREEN;
    [self finishScreen];
    
}

/*
 * Method to get notification, hd image and Night mode enable/Disable state from preferences
 */
-(void)getDataFromPreferences{
    @autoreleasepool {
        NSString *notiifcationKey = PREF_KEY_NOTIFICATION;
        NSString *hdImageKey = PREF_KEY_HD_IMAGE;
        NSString *nightModeKey = PREF_KEY_NIGHT_MODE;
        NSString *listModeKey = PREF_KEY_LIST_MODE;
        
        BOOL switchesState = [userDefaultPreferences getBooleanData:notiifcationKey];
        
        //If notification switch value is true then set selected notification image
        if (switchesState) {
            [self.notificationSwitch setOn:true];
            self.notificationImageView.image = [UIImage imageNamed:@"notification_sel.png"];
        }
        
        switchesState = [userDefaultPreferences getBooleanData:hdImageKey];
        
        //If hd image switch value is true then set selected hd image image
        if (switchesState) {
            [self.hdImageSwitch setOn:true];
            self.hdImageView.image = [UIImage imageNamed:@"hd_image_sel.png"];
        }
        
        switchesState = [userDefaultPreferences getBooleanData:nightModeKey];
        
        //If night mode switch value is true then set selected night mode image
        if (switchesState) {
            [self.nightModeSwitch setOn:true];
            self.nightModeImageView.image = [UIImage imageNamed:@"night_mode_sel.png"];
        }
        
        switchesState = [userDefaultPreferences getBooleanData:listModeKey];
        
        //If night mode switch value is true then set selected night mode image
        if (switchesState) {
            [self.listModeSwitch setOn:true];
            self.listModeImageView.image = [UIImage imageNamed:@"list_mode_sel.png"];
        }
        
        listModeKey = nil;
        notiifcationKey = nil;
        hdImageKey = nil;
        nightModeKey = nil;
        switchesState = nil;
    }
}


/*
 * Method to store notification, hd image and Night mode enable/Disable state into preferences
 */
-(void)storeDataIntoPreferences:(NSInteger)identifier{
    
    //Save notificationSwitch value as per its status.
    if (identifier == NOTIFICATION_BUTTON_TAG) {
        NSString *notiifcationKey = PREF_KEY_NOTIFICATION;
        
        if (self.notificationSwitch.isOn) {
            [userDefaultPreferences setBool:YES forKey:notiifcationKey];
        }
        else{
            [userDefaultPreferences setBool:NO forKey:notiifcationKey];
        }
        notiifcationKey = nil;
    }
    
    //Save hdImageSwitch value as per its status.
    if (identifier == HD_IMAGE_BUTTON_TAG) {
        NSString *hdImageKey = PREF_KEY_HD_IMAGE;
        
        if (self.hdImageSwitch.isOn) {
            [userDefaultPreferences setBool:YES forKey:hdImageKey];
        }
        else{
            [userDefaultPreferences setBool:NO forKey:hdImageKey];
        }
        
        hdImageKey = nil;
    }
    
    //Save nightModeSwitch value as per its status.
    if (identifier == NIGHT_MODE_BUTTON_TAG) {
        NSString *nightModeKey = PREF_KEY_NIGHT_MODE;
        
        if (self.nightModeSwitch.isOn) {
            [userDefaultPreferences setBool:YES forKey:nightModeKey];
        }
        else{
            [userDefaultPreferences setBool:NO forKey:nightModeKey];
        }
        
        nightModeKey = nil;
    }
    
    //Save nightModeSwitch value as per its status.
    if (identifier == LIST_MODE_BUTTON_TAG) {
        NSString *listModeKey = PREF_KEY_LIST_MODE;
        
        if (self.listModeSwitch.isOn) {
            [userDefaultPreferences setBool:YES forKey:listModeKey];
        }
        else{
            [userDefaultPreferences setBool:NO forKey:listModeKey];
        }
        
        listModeKey = nil;
    }
    
}

/*
 * Method to share data
 */
-(void)shareData{
    NSURL *urlString = [NSURL URLWithString:@"http://www.google.com"];
    //  NSString *shareString = @"Check out \"Pollyannaish\" app. I found it best for reading news.";
    UIImage *shareImage = [UIImage imageNamed:@"share.png"];
    [Utils socialShareData:@"Check out \"Pollyannaish\" app. I found it best for reading news." andShareImage:shareImage andShareURL:urlString andPresentViewController:self];
    
}

/*
 * Call back of MFMailComposeViewControllerDelegate
 */
#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self.feedbackAppClass mailComposeController:controller didFinishWithResult:result error:error];
    
}

/*
 * Call back of CustomAlertViewDelegate
 */


-(void)onAlertViewLeftButtonClicked:(int)identifier{
    if (identifier == CUSTOM_ALERT_VIEW_WITH_THREE_BUTTON_IDENTIFIER) {
        //Rate app button
        [self.rateAppClass onAlertViewLeftButtonClicked:identifier];
    }
    
    if (identifier== CUSTOM_ALERT_VIEW_WITH_TWO_BUTTON_IDENTIFIER) {
        
    }
    
}


-(void)onAlertViewCenterButtonClicked:(int)identifier{
    if (identifier == CUSTOM_ALERT_VIEW_WITH_THREE_BUTTON_IDENTIFIER) {
        //Remind me later button
        [self.rateAppClass onAlertViewCenterButtonClicked:identifier];
    }
    
    if (identifier== CUSTOM_ALERT_VIEW_WITH_TWO_BUTTON_IDENTIFIER) {
        
    }
}


-(void)onAlertViewRightButtonClicked:(int)identifier{
    if (identifier == CUSTOM_ALERT_VIEW_WITH_THREE_BUTTON_IDENTIFIER) {
        //No thanks button
        [self.rateAppClass onAlertViewRightButtonClicked:identifier];
    }
    
    if (identifier== CUSTOM_ALERT_VIEW_WITH_TWO_BUTTON_IDENTIFIER) {
        
    }
}

/*
 * Method to set smily in message
 */
-(NSString*)setSmilyMessage{
    
    NSString *bellEmojiString = [NSString stringWithFormat:@"%@",SETTING_SCREEN_COMING_SOON_MESSAGE];
    bellEmojiString = [bellEmojiString stringByAppendingFormat:@" %@", @"\U0001F60A"];
    return bellEmojiString;
}


-(void)finishScreen{
    [[ApplicationController getInstance]handleEvent:EVENT_ID_FINISH_SCREEN];
}

-(void)onScreenPopedUp{
    NSLog(@"On screen poped up");
}

-(void)onTopScreenFinished{
    NSLog(@"On top screen finished");
}

-(void)destroy{
    selectedImagesArray = nil;
    unSelectedImagesArray = nil;
    self.SCROLLVIEW= nil;
    self.notificationSwitch= nil;
    self.hdImageSwitch= nil;
    self.nightModeSwitch= nil;
    self.notificationImageView= nil;
    self.hdImageView= nil;
    self.nightModeImageView= nil;
    self.shareAppImageView= nil;
    self.rateAppImageView= nil;
    self.feedBackImageView= nil;
    self.termConditionImageView= nil;
    self.privacyImageView= nil;
    self.shareAppButton= nil;
    self.rateAppButton= nil;
    self.feedBackButton= nil;
    self.termConditionButton= nil;
    self.privacyButton= nil;
    self.returnButton= nil;
    self.previousSelectedButton= nil;
    self.previousSelectedImageView= nil;
    self.feedbackAppClass= nil;
    self.rateAppClass= nil;
    self.mFMailComposeViewControllerDelegate= nil;
    self.mailViewController= nil;
    self.customAlertViewWithThreeButtons= nil;
    self.customAlertView = nil;
    
}





@end
