//
//  Utils.h
//  IosApplicationFrameworkProject
//
//  Created by test on 16/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ApplicationController.h"
#import "NewsDao.h"
#import "NewsListModel.h"
#import "UnReadNewsClass.h"

@interface Utils : NSObject


#pragma METHODS TO CONVERT HEXADECIMAL VALUE TO UICOLOR.
+ (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;

+ (unsigned int)intFromHexString:(NSString *)hexStr;

+ (void)showCenterViewWithShadow:(BOOL)value withOffset:(double)offset view:(UIView *)containerView;

+(void)setBorderToView:(UIView*)view;

+(void)changeSelectedViewColor:(UIButton*)selectedButton andImageView:(UIImageView*)selectedImageView unSelectedImagesArray:(NSArray*)unSelectedImagesArray selectedImagesArray:(NSArray*)selectedImagesArray;

+(void)socialShareData:(NSString*)shareString andShareImage:(UIImage *)shareImage andShareURL:(NSURL *)shareUrl andPresentViewController:(UIViewController*)presentViewController;

+(void)selectCurrentView:(UIButton*)selectedButton andImageView:(UIImageView*)selectedImageView selectedImagesArray:(NSArray*)selectedImagesArray;

+(NewsDao*)getDaoFromCategory:(NSString*)selectedCategory andNewsIndex:(int)pageIndex;

+(NSMutableArray*)getArrayFromCategory:(NSString*)selectedCategory;

+(void)takePageScreenShots:(UIView*)view andPresentViewController:(UIViewController*)presentViewController;

+(void)setUpperViewBorder:(UIView*)view andBorderColour:(UIColor*)colour;
@end
