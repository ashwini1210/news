//
//  TestScreenViewController.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/23/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "TestScreenViewController.h"
#import "ApplicationController.h"

@implementation TestScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [[NSBundle mainBundle] loadNibNamed:@"TestScreenViewController" owner:self options:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    NSLog(@"Test Initialized");
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void) update{
    
    
}

- (IBAction)onButtClick:(id)sender {
    NSLog(@"On Test Button Cliked>>>>");
    
}

- (IBAction)goToNextScreenButtonClicked:(id)sender {
   
    [[ApplicationController getInstance] handleEvent:EVENT_ID_CUSTOM_PAGE_VIEW_SCREEN];

}

- (IBAction)backButtonClicked:(id)sender {
    [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_SCREEN];
}
@end
