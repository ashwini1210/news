//
//  DetailNewsScreenViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 04/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DetailNewsViewController.h"
#import "LocalModel.h"
#import "NewsListModel.h"
#import "Utils.h"

@interface DetailNewsScreenPagerViewController : BaseViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate>
{
    NSMutableArray *newsDataArray;
    int pageIndex;
    LocalModel *localModel;
    NewsListModel *newsListModel;
    BOOL isTitleBarVisible;
    
}
@property (strong, nonatomic) IBOutlet UIView *parentContainerView;
@property (strong, nonatomic) UIPageViewController *uiViewPagerViewController;
@property (strong, nonatomic) DetailNewsViewController *detailNewsViewController;
@property (strong, nonatomic) NSMutableDictionary* newsDataDictionary;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UIView *titleBarView;
- (IBAction)shareButtonClickEvent:(id)sender;

@end
