//
//  FeedbackAppClass.h
//  IosApplicationFrameworkProject
//
//  Created by test on 25/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import "SettingViewController.h"

@interface FeedbackAppClass : NSObject
{
   
    SettingViewController *settingVController;
    NSString * mailBody;
}
-(void)initialize:(SettingViewController*)settingViewController;
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error;


@end
