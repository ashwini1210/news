//
//  BaseViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 08/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 * Method calls when response from server comes. It override update method.
 */
-(void)update:(int)identifier{
    NSLog(@"Update of basevc : identifier: %d",identifier);

}

-(void)onScreenPopedUp{
      NSLog(@"onScreenPopedUp of basevc");
}

-(void) update{
    NSLog(@"update of basevc");

}
/*
 * Gets callback on top screen finished
 */
-(void) onTopScreenFinished{
    NSLog(@"onTopScreenFinished of basevc");
}

/**
 * Destroy View and Model Data for the given Screen.
 *
 */
-(void) destroy{
      NSLog(@"destroy of basevc");
}
@end
