//
//  NotificationClass.m
//  IosApplicationFrameworkProject
//
//  Created by test on 07/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "NotificationClass.h"

@implementation NotificationClass

/*
 * Integer value for timer sleep to 1 hour
 */
int sleepTimer = 3600.0;

/*
 * Boolean value to keep thread running continuesly based on value.
 */
bool updateNotification=true;


-(id)init{
    self = [super init];
    //Create instance of UILocalNotification
    localNotification = [[UILocalNotification alloc] init];
    [self initializeThread];
    return self;
}

/*
 * Method to initialize thread
 */
-(void)initializeThread{
    timerThread = [[NSThread alloc] initWithTarget:self selector:@selector(throwNotification) object:nil];
}


-(void)initialize{
    if (![ timerThread isExecuting]) {
        updateNotification = YES;
        [timerThread start];
    }
    [self initializeInstances];
    
}


/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    
    localModel = [[[ApplicationController getInstance]getModelFacade]getLocalModel];
    allNewsDataArray = localModel.newsListModel.allNewsDataArray;
}

/*
 * Method to throw Notification after every 60 sec(1 min)
 */
-(void)throwNotification{
    //here continuesly updating the notification after same interval of time ie. 30 sec
    while (updateNotification) {
        [self checkNotificationSwitchState];
        [NSThread sleepForTimeInterval:sleepTimer];
        
    }
}

/*
 * Method to get notification witch enable/Disable state from preferences
 */
-(void)checkNotificationSwitchState{
    @autoreleasepool {
        
        UserDefaultPreferences *userDefaultPreferences = [UserDefaultPreferences getInstance];
        NSString *notiifcationKey = PREF_KEY_NOTIFICATION;
        if (userDefaultPreferences!=nil) {
            BOOL switchesState = [userDefaultPreferences getBooleanData:notiifcationKey];
            
            //If notification switch is ON then set Notification
            if (switchesState) {
                [self setupLocalNotifications];
                //[self checkForScheduleNotification];
            }
            //If notification switch is OFF then cancel Notification
            else{
                [self cancelLocalNotifications];
            }
            
        }
        userDefaultPreferences = nil;
        notiifcationKey = nil;
    }
    
}

/*
 * Method to send local notification in app
 */
- (void)setupLocalNotifications {
    NSLog(@"setupLocalNotifications");
    @autoreleasepool {
        
        // current time
        NSDate *now = [NSDate date];
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar] ;
        
        NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:now];
        
        //Display Notification after every 1 hour
        [components setHour:1];
        
        localNotification.fireDate = [calendar dateFromComponents:components];
        
        //localNotification.repeatInterval = NSHourCalendarUnit;
        //localNotification.repeatInterval = NSMinuteCalendarUnit;
        
        //Get News Dao of randomly selected index
        NewsDao *newsDao = [self getNewsDao];
        
        //Get news Headline of randomly selected index to set into notification body
        NSString *newsHeadline = [self setNotificationHeadline:newsDao];
        
        //Body/Message of Notification i.e Headline of News
        localNotification.alertBody = newsHeadline;
        
        //Sound when notification arrive
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber]+1;
        
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        //get a newsId to be Set into userInfo
        newsId = [self setNewsId:newsDao];
        
        // Associate a dictionary to identify to gather the information about the notification
        //Set News Id into userInfo
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:newsId
                                                             forKey:NOTIFICATION_KEY_NEWS_ID];
        
        //attach custom data to the notification
        localNotification.userInfo = userDict;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        newsDao = nil;
        now = nil;
        calendar = nil;
        components = nil;
        newsHeadline = nil;
        
    }
}

/*
 * Method to get NewsDao of Randomly generated index
 */

-(NewsDao*)getNewsDao{
    
    @autoreleasepool {
        NewsDao* newsDao =nil;
        if (allNewsDataArray!=nil) {
            //Get random index of allNewsDataArray
            NSUInteger randomIndex = arc4random() % [allNewsDataArray count];
            
            //Get newsdao from random index of allNewsDataArray
            newsDao  = [allNewsDataArray objectAtIndex:randomIndex];
            
        }
        return newsDao;
    }
}

/*
 * Method to set News Headline into Notification Body
 */
-(NSString *)setNotificationHeadline:(NewsDao*)newsDao{
    @autoreleasepool {
        NSString *newsHeadline = nil;
        newsHeadline = newsDao.newsHeading;
        return newsHeadline;
    }
}

/*
 * Method to set News Headline ID into Notification
 */
-(NSString *)setNewsId:(NewsDao*)newsDao{
    @autoreleasepool {
        NSString *newsHeadlineID = nil;
        newsHeadlineID = [NSString stringWithFormat:@"%ld",(long)newsDao.newsId];
        return newsHeadlineID;
    }
}

/*
 * Method to cancel notification
 */
-(void)cancelLocalNotifications{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *notificationArray = [app scheduledLocalNotifications];
    
    if (notificationArray!=nil) {
        for (int i=0; i<[notificationArray count]; i++)
        {
            UILocalNotification* notification = [notificationArray objectAtIndex:i];
            if (notification!=nil) {
                //If present then Cancelling local notification
                [app cancelLocalNotification:notification];
                [app cancelAllLocalNotifications];
            }
        }
    }
    
}


/*
 * Method to switch user into content which is present in Notification Body
 */
-(void)switchToLocalNotificationNews:(UILocalNotification *)notification {
    @autoreleasepool {
        NSDictionary *userInfo = [notification userInfo];
        
        NSInteger newsID= [[userInfo objectForKey:NOTIFICATION_KEY_NEWS_ID]integerValue];
        for (int i=0; i<[allNewsDataArray count]; i++) {
            
            NewsDao *newsDao = [allNewsDataArray objectAtIndex:i];
            NSInteger tempNewsID = newsDao.newsId;
            
            if (newsID == tempNewsID) {
                int index = [allNewsDataArray indexOfObject:newsDao];
                
                if (self.homeScreenViewController!=nil) {

                    [self.homeScreenViewController onNotificationClicked:index];
                    [self.homeScreenViewController refreshData];
                }
                break;
            }
            
            newsDao = nil;
        }
        
    }
}



//NOT IN USE METHOD

/*
 * Method to check if this notification is exist in schedule Notification array. If exist delete it
 */
-(void)checkForScheduleNotification{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *notificationArray = [app scheduledLocalNotifications];
    
    if (notificationArray!=nil) {
        for (int i=0; i<[notificationArray count]; i++)
        {
            UILocalNotification* notification = [notificationArray objectAtIndex:i];
            if (notification!=nil) {
                
                //Get Customize data saved in Notification's userInfo
                NSDictionary *userInfoCurrent = notification.userInfo;
                
                //EExtract newsID value form it's key
                NSString *notificationNewsId=[userInfoCurrent objectForKey:NOTIFICATION_KEY_NEWS_ID];
                
                NSLog(@"News Id  save into UserInfo Notification %@",newsId);
                NSLog(@"News Id get from UserInfo Notification %@",notificationNewsId);
                
                
                //Check if newsId is already exist in Notification's userInfo
                if ([notificationNewsId isEqualToString:newsId])
                {
                    
                    NSLog(@"CancelLocalNotifications");
                    
                    //If present then Cancelling local notification
                    [app cancelLocalNotification:notification];
                    
                }
                //If not present then schedule local notification
                else{
                    [self setupLocalNotifications];
                }
                
                notificationNewsId = nil;
                userInfoCurrent = nil;
            }
            
            notification= nil;
        }
        
    }
    //If not present then schedule local notification
    else{
        
        //It will call first time bcz notificationArray is nill
        [self setupLocalNotifications];
    }
    
    notificationArray = nil;
    app = nil;
}





@end
