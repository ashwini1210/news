//
//  CustomTableViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 13/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "CustomTableViewController.h"


@implementation CustomTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[NSBundle mainBundle] loadNibNamed:@"CustomTableViewController" owner:self options:nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 6;
}


-(SampleTableViewCell *)createInputCellForIndexPath:(NSIndexPath *)indexPath  withTable:(UITableView *)__tableView
{
    static NSString *CellIdentifier = @"SampleTableViewCell";
    
    SampleTableViewCell *cell = (SampleTableViewCell *)[__tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SampleTableViewCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (SampleTableViewCell *)currentObject;
                break;
            }
        }
    }
    
    
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        SampleTableViewCell *cell = [self createInputCellForIndexPath:indexPath withTable:self.tableView];
        
        if(indexPath.row % 2 == 0)
        {
            cell.titleLabel.text = @"Next >";
            
            return cell;
        }
        else{
            cell.titleLabel.text = @"< Back";
            
            return cell;
            
        }
        
    return nil;

}

- (void)tableView:(UITableView *)localtableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [localtableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row % 2 == 0)
    {
        [[ApplicationController getInstance] handleEvent:EVENT_ID_CUSTOM_COLLECTION_VIEW_SCREEN];
    }
    else
    {
        [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_SCREEN];
    }
    
}


-(void) update{
    
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goToNextButtonPressed:(id)sender {
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_CUSTOM_PAGE_VIEW_SCREEN];
}

- (IBAction)backButtonPressed:(id)sender {
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_SCREEN];
}
@end
