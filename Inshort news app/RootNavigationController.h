//
//  RootNavigationController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 10/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Child1NavigationController.h"
#import "ApplicationController.h"

@interface RootNavigationController : UIViewController<AbstractViewController>
{
    UINavigationController *rootNavigationController;
    Child1NavigationController *child1NavigationController;
}
- (IBAction)backButtonPressed:(id)sender;
- (IBAction)goToNextScreenButtonClicked:(id)sender;
@property (strong, atomic) UINavigationController *rootNavigationController;;
@end
