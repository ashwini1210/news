//
//  FeedbackAppClass.m
//  IosApplicationFrameworkProject
//
//  Created by test on 25/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "FeedbackAppClass.h"
#import "AppConstants.h"

@implementation FeedbackAppClass

/*
 * initialize class
 */
-(void)initialize:(SettingViewController*)settingViewController{
    
    settingVController = settingViewController;
    mailBody = [self setMailBody];
    [self sendFeedbackMail];
    
}

/*
 * Method to send mail for Feedback
 */
-(void)sendFeedbackMail{
    
    settingVController.mailViewController = [[MFMailComposeViewController alloc]init];
    settingVController.mailViewController.mailComposeDelegate = settingVController.mFMailComposeViewControllerDelegate;
    [settingVController.mailViewController setToRecipients:@[@"contact@pollyannaish.com"]];
    [settingVController.mailViewController setSubject:FEEDBACK_SCREEN_MAIL_TITLE];
    [settingVController.mailViewController setMessageBody:mailBody isHTML:NO];
    [settingVController presentViewController:settingVController.mailViewController animated:YES completion:nil];
    
    
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self displayError:result error:error];
    
    
    UIAlertView *alert;
    switch (result)
    {
        case MFMailComposeResultSaved:
            
            alert = [[UIAlertView alloc] initWithTitle:FEEDBACK_SCREEN_MAIL_SAVED message:FEEDBACK_SCREEN_MAIL_SAVED_MESSAGE delegate:nil cancelButtonTitle:CUSTOM_ALERT_OK_BUTTON_TEXT otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultSent:
            alert = [[UIAlertView alloc] initWithTitle:FEEDBACK_SCREEN_MAIL_SENT message:FEEDBACK_SCREEN_MAIL_SENT_MESSAGE delegate:nil cancelButtonTitle:CUSTOM_ALERT_OK_BUTTON_TEXT otherButtonTitles:nil, nil];
            [alert show];
            break;
        case MFMailComposeResultFailed:
            alert = [[UIAlertView alloc] initWithTitle:FEEDBACK_SCREEN_MAIL_FAILED message:FEEDBACK_SCREEN_MAIL_FAILED_MESSAGE delegate:nil cancelButtonTitle:CUSTOM_ALERT_OK_BUTTON_TEXT otherButtonTitles:nil, nil];
            [alert show];
            break;
        default:
            break;
                
    }
    
    // Close the Mail Interface
    [settingVController dismissViewControllerAnimated:YES completion:nil];
    alert = nil;
    
}


/*
 * Method to display error if any
 */
-(void)displayError:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    
}

/*
 * Method to set Mail body content for feedback
 */
-(NSString*)setMailBody{
    NSString *mailBody=@"";
    NSString* temparyVariable= nil;
    
    temparyVariable = [self getDeviceId];
    if (temparyVariable!=nil) {
        mailBody = [NSString stringWithFormat:@"Device Id: ios_%@",temparyVariable];
    }
    
    temparyVariable = [self getDeviceName];
    if (temparyVariable!=nil) {
        //mailBody =[mailBody stringWithFormat:@"\nDevice Name: %@",temparyVariable];
        mailBody =[mailBody stringByAppendingFormat:@"\nDevice Name: %@",temparyVariable];
    }
    
    temparyVariable = [self getSystemVersion];
    if (temparyVariable!=nil) {
        mailBody =[mailBody stringByAppendingFormat:@"\nSystem Version: %@",temparyVariable];
    }
    
    temparyVariable = [self getAppVersion];
    if (temparyVariable!=nil) {
        mailBody =[mailBody stringByAppendingFormat:@"\nApp Version: %@",temparyVariable];
    }
    
    temparyVariable = [self getAppLanguage];
    if (temparyVariable!=nil) {
        mailBody =[mailBody stringByAppendingFormat:@"\nApp Language: %@",temparyVariable];
    }
    
    temparyVariable = [self getWarningNote];
    if (temparyVariable!=nil) {
        mailBody =[mailBody stringByAppendingFormat:@"\n %@",temparyVariable];
    }
    
    temparyVariable = nil;
    return mailBody;
}

/*
 * Method to get current device id
 */
-(NSString*)getDeviceId{
    NSString* deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"device Identifier is : %@", deviceIdentifier);
    return deviceIdentifier;
    
}

/*
 * Method to get current device name
 */
-(NSString*)getDeviceName{
    NSString* deviceName =  [[UIDevice currentDevice] name];
    return deviceName;
}

/*
 * Method to get current device version
 */
-(NSString*)getSystemVersion{
    NSString *iOSVersion = [[UIDevice currentDevice] systemVersion];
    return iOSVersion;
}

/*
 * Method to get app version
 */
-(NSString*)getAppVersion{
    NSString * appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return appVersion;
    
}

/*
 * Method to get app Language
 */
-(NSString*)getAppLanguage{
    NSString * appLanguage = @"English";
    return appLanguage;
    
}

-(NSString*)getWarningNote{
    NSString *warningNote = @"--- Please don't edit anything above this line, to help us serve you better---";
    return warningNote;
}



@end
