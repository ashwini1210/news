//
//  NewsListModel.h
//  IosApplicationFrameworkProject
//
//  Created by test on 10/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "BaseModel.h"
#import "NewsDao.h"

@interface NewsListModel : BaseModel
{
    
}
//@property(strong,nonatomic)NSMutableArray* newsListDataArray;
@property(strong,nonatomic)NSMutableArray* allNewsDataArray;
@property(strong,nonatomic)NSMutableArray* topStoryDataArray;
@property(strong,nonatomic)NSMutableArray* trendingDataArray;
@property(strong,nonatomic)NSMutableArray* indiaDataArray;
@property(strong,nonatomic)NSMutableArray* buisnessDataArray;
@property(strong,nonatomic)NSMutableArray* politicsDataArray;
@property(strong,nonatomic)NSMutableArray* sportsDataArray;
@property(strong,nonatomic)NSMutableArray* technologyDataArray;
@property(strong,nonatomic)NSMutableArray* startupDataArray;
@property(strong,nonatomic)NSMutableArray* entertainmentDataArray;
@property(strong,nonatomic)NSMutableArray* internationalDataArray;
@property(strong,nonatomic)NSMutableArray* healthDataArray;
@property(strong,nonatomic)NSMutableArray* bookMarkArray;
@property(strong,nonatomic)NSMutableArray* unreadNewsArray;
@property(strong,nonatomic) NSString* currentSelectedCategory;
-(NSMutableArray*)getCategoriesDataArray:(NSString*)newsCategory;


@end
