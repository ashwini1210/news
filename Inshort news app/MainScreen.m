//
//  MainScreen.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/12/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "MainScreen.h"
#import "ApplicationController.h"
#import "AppConstants.h"
#import "GetAllUsersModel.h"
#import "SignUpModel.h"

@implementation MainScreen

- (void)viewDidLoad {
    NSLog(@"Main Screen View Load");
    [super viewDidLoad];
    [self.sampleImage setUserInteractionEnabled:YES];
    [self setListenerToImageView];
    
    
//    rootNavigationController = [[RootNavigationController alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setListenerToImageView{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onImageClick)];
    tap.cancelsTouchesInView = YES;
    tap.numberOfTapsRequired = 1;
    [self.sampleImage addGestureRecognizer:tap];
    
}

- (IBAction)backButtonPressed:(id)sender {
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_SCREEN];
}

-(void) onImageClick{
    NSLog(@"Splash OnAppleImageClick");
//    [[ApplicationController getInstance] handleEvent:EVENT_ID_TEST_SCREEN];
//    SignUpModel *signUpModel = [SignUpModel alloc];
//    [signUpModel initialize];
//    GetAllUsersModel *getAllModel = [GetAllUsersModel alloc];
//    [getAllModel initialize];
    [[ApplicationController getInstance]handleEvent:EVENT_ID_ROOT_NAVIGATION_SCREEN];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
/**
 * To initialize the Views or Screens, Models
 *
 */
-(void) initialize{
    NSLog(@"MAIN initialize");
    
}


/**
 * To reinitialize the View or Screen. This is for some cases when user
 * implement freeResource to optimize memory issue.
 */


-(void) reInitialize{
    NSLog(@"MAIN reInitialize");
    
}

/**
 * To free only View releated stuff.
 *
 */
-(void) freeResources{
    NSLog(@"MAIN freeResources");
    
}

/**
 * Destroy View and Model Data for the given Screen.
 *
 */
-(void) destory{
    NSLog(@"MAIN destory");
    
}

/**
 * This function get called whenever AbstractView redisplay on device screen
 *
 */

-(void) enable{
    NSLog(@"MAIN enable");
    
}


/**
 * This function get called whenever AbstractView overlapped by another
 * AbstractView
 */

-(void) disbale{
    NSLog(@"MAIN disbale");
    
}



/**
 * To notify the AbstarctView, that there is an interruption. So
 * AbstractView can handle all the required condition before going
 * application in background.
 *
 */

-(void) showNotify{
    NSLog(@"MAIN hideNotify");
    
}

/**
 * To notify the AbstractView that application will be visible on the screen
 * after calling this method.
 */

-(void) hideNotify{
    NSLog(@"MAIN hideNotify");
    
}

/**
 * To update a view
 */

-(void) update{
    NSLog(@"MAIN update");
}
//-(void)loadView{
//    [super loadView];
//    NSLog(@"MAIN Loadview");
//}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"MAIN viewDidAppear");
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSLog(@"MAIN viewDidDisappear");
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"MAIN viewWillAppear");
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSLog(@"MAIN viewWillDisappear");
}
-(void) viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    NSLog(@"MAIN viewWillLayoutSubviews");
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    NSLog(@"MAIN viewDidLayoutSubviews");
}


- (IBAction)clickMeButtonTapped:(id)sender {
//    rootNavigationController = [RootNavigationController alloc];
//    UINavigationController *uiNavigationController = [[UINavigationController alloc] initWithRootViewController:rootNavigationController];
//    [self presentViewController:uiNavigationController animated:YES completion:nil];
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_ROOT_NAVIGATION_SCREEN];
}
@end
