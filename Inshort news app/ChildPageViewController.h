//
//  ChildPageViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 14/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ChildPageViewController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)goToNextScreenButtonClicked:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

@end
