//
//  DetailNewsScreenViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 04/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "DetailNewsScreenPagerViewController.h"
#import "AppConstants.h"
#import "NewsDao.h"
#import "ApplicationController.h"

@interface DetailNewsScreenPagerViewController ()

@end

@implementation DetailNewsScreenPagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeInstances];
    [self getDataFromDictionary];
    [self setAdapterInPagerController];
    [self setDataIntoCategoryLabel];
  
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    newsDataArray = [[NSMutableArray alloc]init];
    localModel = [[[ApplicationController getInstance] getModelFacade] getLocalModel];
    newsListModel = localModel.newsListModel;
    isTitleBarVisible = true;
    
}



/*
 * Method to get data from dictionary
 */
-(void)getDataFromDictionary{
    newsDataArray = [self.newsDataDictionary objectForKey:SELECTED_CATEGORY_ARRAY];
    pageIndex = [[self.newsDataDictionary valueForKey:SELECTED_CELL_INDEX_KEY] intValue];
}

/*
 * Method to set selected Category name into Category Label.
 */
-(void)setDataIntoCategoryLabel{
    NSString *selectedCategory = newsListModel.currentSelectedCategory;
    
    if ([selectedCategory isEqual:TOP_STORIES_CATEGORY_TYPE]) {
        self.categoryLabel.text = @"Top Stories";
    }
    else if ([selectedCategory isEqual:ALL_NEWS_CATEGORY_TYPE]){
        self.categoryLabel.text = @"All News";
        
    }
    else if ([selectedCategory isEqual:TRENDING_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Trending News";
    }
    
    else if ([selectedCategory isEqual:INDIA_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Indian News";
    }
    
    else if ([selectedCategory isEqual: BUISNESS_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Buisness News";
    }
    
    else if ([selectedCategory isEqual:POLITICS_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Politics News";
    }
    
    else if ([selectedCategory isEqual: SPORTS_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Sports News";
    }
    
    else if ([selectedCategory isEqual: TECHNOLOGY_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Technology News";
    }
    else if ([selectedCategory isEqual:STARTUP_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Startup News";
    }
    
    else if ([selectedCategory isEqual:ENTERTAINMENT_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Entertainment News";
    }
    
    else if ([selectedCategory isEqual:INTERNATIONAL_CATEGORY_TYPE]){
        self.categoryLabel.text = @"International News";
        
    }
    else if ([selectedCategory isEqual:HEALTH_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Health News";
        
        
    }
    else if ([selectedCategory isEqual:BOOKMARK_CATEGORY_TYPE]){
        self.categoryLabel.text = @"Bookmark News";
        
        
    }
}


/*
 * Method to set adaoter to pager screen viewcontroller
 */
-(void)setAdapterInPagerController{
    self.uiViewPagerViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.uiViewPagerViewController.dataSource = self;
    [[self.uiViewPagerViewController view] setFrame:[[self parentContainerView] bounds]];
    
    self.detailNewsViewController = [self viewControllerAtIndex:pageIndex];
    
    NSArray *viewControllers = [NSArray arrayWithObject: self.detailNewsViewController ];
    
    [self.uiViewPagerViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.uiViewPagerViewController];
    [[self parentContainerView] addSubview:[self.uiViewPagerViewController view]];
    [self.uiViewPagerViewController didMoveToParentViewController:self];
    viewControllers = nil;
}


/*
 * Method to initialize HomeScreenViewControllerAdapters and set data into its views
 */
- (DetailNewsViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    DetailNewsViewController *detailNewsViewController = [[DetailNewsViewController alloc] initWithNibName:@"DetailNewsViewController" bundle:nil];
    detailNewsViewController.index = index;
    [self setDataIntoPagerViews:detailNewsViewController index:index];
    return detailNewsViewController;
    
}

/*
 * Method to set data into pager views
 */
-(void)setDataIntoPagerViews:(DetailNewsViewController *)detailNewsViewController index:(NSUInteger) index{
    int newsindex = (int)index;
    NewsDao *newsDao = [self getNewsByIndex:newsindex];
    detailNewsViewController.newsDao = newsDao;
    
    
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(DetailNewsViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(DetailNewsViewController *)viewController index];
    
    index++;
    
    if (index == [newsDataArray count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

/*
 * Gets the news for provided index if avaliable in list.
 */
-(NewsDao*)getNewsByIndex:(int)index{
    if(newsDataArray!=nil && [newsDataArray count]!=0){
        @try{
            return [newsDataArray objectAtIndex:index];
        }@catch(NSException *nsRangeException){
            //if the array index is out of range.
            return nil;
        }
        
    }
    return nil;
}

- (IBAction)backButtonClickEvent:(id)sender {
    [[ApplicationController getInstance]handleEvent:EVENT_ID_FINISH_SCREEN];
}

- (IBAction)shareButtonClickEvent:(id)sender {
    [Utils takePageScreenShots:self.parentContainerView andPresentViewController:self];
}


-(void)onTopScreenFinished{
    
}

-(void)onScreenPopedUp{
    NSLog(@"On screen poped up");
}


//-(void)destroy{
//    newsDataArray = nil;
//    self.newsDataDictionary = nil;
//    self.parentContainerView = nil;
//    self.uiViewPagerViewController= nil;
//    self.detailNewsViewController= nil;
//    localModel = nil;
//    newsListModel = nil;
//}

@end
