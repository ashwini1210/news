//
//  CustomTableViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 13/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "ApplicationController.h"
#import "SampleTableViewCell.h"

@interface CustomTableViewController : UITableViewController <AbstractViewController, UITableViewDataSource, UITableViewDelegate>
- (IBAction)goToNextButtonPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

@end
