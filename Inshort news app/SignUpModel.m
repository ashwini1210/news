//
//  SignUpModel.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 3/17/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "SignUpModel.h"
#import "HttpRequestHandlerImpl.h"
#import "AppConstants.h"
@implementation SignUpModel

@synthesize userName;
@synthesize password;
@synthesize emailAdd;

-(void) initialize{
    [super initialize];
    NSLog(@"Sign Up Screen Model : Initialize");
    [self registerUser:requestData];
}

/*
 Sends the request to sign Up user to server.
 */
-(void) registerUser:(NSData*) requestBody{
    
    [httpRequestHandlerImpl setServiceURL:URL_CHECK_SIGN_UP];
    [httpRequestHandlerImpl setMethodType:HTTP_PUT];
//    responseData = [httpRequestHandlerImpl execute:requestBody];
//    
//    NSLog(@"Response: %@ ",responseData);
//    //    NSLog(@" Error %@", error);
//    NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//    NSLog(@" Response String: %@",str);
    //informing the UI to update there respective display or functionality.
    [self informView];
}

-(void)setUserName:(NSString*)userName{
    self-> userName = userName;
    
}
-(void)setPassword:(NSString*)password{
    self-> password = password;
}
-(void)setEmailAdd:(NSString*)emailId{
    self->emailAdd = emailId;
}
-(void)setMob:(long)mobileNum{
    self -> mobNum = mobileNum;
}

-(NSData*)getSignUpRequestBody{
    NSString *body = @"";
//    [body stringByAppendingFormat:@"{\"username\":\"%@\",",userName];
//    [body stringByAppendingFormat:@"{\"username\":\"%@\",",userName];
//    [body stringByAppendingFormat:@"{\"username\":\"%@\"",userName];
//    [body stringByAppendingFormat:@"{\"username\":\"%@\"",userName];
//     \,\"password\":\"%@\"\,\"email\":\"%@\",\"mobile_number\":\"%@\", \"user_type_id\":%@}"];
//     = @"{\"username\":\"ashish1\",\"password\":\"1234567\",\"email\":\"ashish1@syslogic.in\",\"mobile_number\":\"+919860677800\", \"user_type_id\":1}";
    
    NSData *data = [body dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}
@end
