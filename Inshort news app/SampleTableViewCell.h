//
//  SampleTableViewCell.h
//  IosApplicationFrameworkProject
//
//  Created by test on 14/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SampleTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
