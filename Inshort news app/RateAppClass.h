//
//  RateAppClass.h
//  IosApplicationFrameworkProject
//
//  Created by test on 25/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingViewController.h"

@interface RateAppClass : NSObject
{
 
    SettingViewController *settingVController;
}

-(id)initialize:(SettingViewController*)settingViewController;

-(void)onAlertViewLeftButtonClicked:(int)identifier;

-(void)onAlertViewCenterButtonClicked:(int)identifier;

-(void)onAlertViewRightButtonClicked:(int)identifier;

@end
