//
//  CustomPageViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 13/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "CustomPageViewController.h"


@implementation CustomPageViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[NSBundle mainBundle] loadNibNamed:@"CustomPageViewController" owner:self options:nil];
    // Do any additional setup after loading the view from its nib.
    
    [self initializePageController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) initializePageController{
    
    self.dataSource = self;
    
    ChildPageViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
//    [self initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];

    
    [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}





- (ChildPageViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    ChildPageViewController *childViewController = [[ChildPageViewController alloc] init];
    childViewController.index = index;
    
    return childViewController;
    
}
//-(instancetype)initWithTransitionStyle:(UIPageViewControllerTransitionStyle)style navigationOrientation:(UIPageViewControllerNavigationOrientation)navigationOrientation options:(NSDictionary *)options{
//    return [super initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma UIPageViewController Stubs

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ChildPageViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ChildPageViewController *)viewController index];
    
    index++;
    
    if (index == 5) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 5;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}



-(void) update{
    
    
}

- (IBAction)goToNextScreenButtonPressed:(id)sender {
    
//    [[ApplicationController getInstance] handleEvent:EVENT_ID_CUSTOM_COLLECTION_VIEW_SCREEN];
}

- (IBAction)backButtonPressed:(id)sender {
    
//    [self dismissViewControllerAnimated:YES completion:nil];
//        [[ApplicationController getInstance] handleEvent:EVENT_ID_ON_SCREEN_FINISH];

//    [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_SCREEN];
}
@end
