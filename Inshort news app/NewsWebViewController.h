//
//  NewsWebViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 11/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomAlertView.h"
#import "NewsDao.h"

@interface NewsWebViewController : BaseViewController<CustomAlertViewDelegate>
{
    CustomAlertView *customProgressDialog;
    CustomAlertView *errorCustomAlertView;
}
@property (strong, nonatomic) IBOutlet UIWebView *newsWebView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonClickEvent:(id)sender;
@property (strong,nonatomic) NewsDao *newsDo;
@end
