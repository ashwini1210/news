//
//  HomeScreenViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 08/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "BaseViewController.h"
#import "NewsDao.h"
#import "UserDefaultPreferences.h"
#import "LocalModel.h"
#import "NewsListModel.h"
#import "HomeScreenViewController.h"


@class BookMarkClass;
@class HomeScreenViewController;

@interface HomeScreenViewControllerAdapters : BaseViewController
{
    UserDefaultPreferences *userDefaultPreferences;
    BookMarkClass *bookMarkClass;
    LocalModel *localModel;
    NewsListModel *newsListModel;


}
@property (strong, nonatomic) IBOutlet UIImageView *newsImageView;
@property (strong, nonatomic) IBOutlet UILabel *newsHeadlineLabel;
@property (strong, nonatomic) IBOutlet UITextView *newsContentLabel;
@property (strong, nonatomic) IBOutlet UILabel *authorAndPostDayLabel;
@property (strong,nonatomic) NewsDao *newsDo;
@property (assign, nonatomic) NSInteger pageIndex;

@property (strong, nonatomic) IBOutlet UIButton *newsLinkButton;
- (IBAction)newsLinkButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *newsHeadlineButton;
- (IBAction)newsHeadlineClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *newsVideoButton;
- (IBAction)newsVideoButtonClickedEvent:(id)sender;

-(void)setDataIntoViews;
- (IBAction)shareButtonClickEvent:(id)sender;
-(void)getDataFromPreferences;

@property (strong, nonatomic) UIActivityViewController *activityViewController;
@property (strong, nonatomic) HomeScreenViewController *homeScreenViewController;
@property (strong, nonatomic) IBOutlet UIView *moreNewsView;
@property (strong, nonatomic) IBOutlet UIView *newsTextBodyView;
//@property (strong, nonatomic) IBOutlet UIView *separatorView;
@property (strong, nonatomic) IBOutlet UILabel *sortByLabel;
@property (strong, nonatomic) IBOutlet UILabel *moreNewsLabel;
@property (strong, nonatomic) IBOutlet UIView *parentView;
@property (strong, nonatomic) IBOutlet UIView *firstMainView;
@property (strong, nonatomic) IBOutlet UILabel *moreNewsLinkText;
@property (strong, nonatomic) IBOutlet UIView *moreAtWebView;
@property (strong, nonatomic) IBOutlet UIView *moreAtVideoView;
@property (strong, nonatomic) IBOutlet UILabel *youTubeTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *videoAtText;

@property (strong, nonatomic) NSString *videoID;

@end
