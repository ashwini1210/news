//
//  Child2NavigationController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 10/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "Child2NavigationController.h"

@interface Child2NavigationController ()

@end

@implementation Child2NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSBundle mainBundle] loadNibNamed:@"Child2NavigationController" owner:self options:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController]) {
        [[ApplicationController getInstance] handleEvent:EVENT_ID_ON_SCREEN_FINISH];
        NSLog(@"Back pressed");
    }
}
-(void)update{
    
    
}

- (IBAction)goToNextScreenButtonClicked:(id)sender {
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_TEST1_VIEW_CONTROLLER];
}

- (IBAction)backButtonPressed:(id)sender {
}
@end
