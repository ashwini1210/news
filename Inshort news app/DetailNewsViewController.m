//
//  DetailNewsViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 04/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "DetailNewsViewController.h"
#import "ApplicationController.h"
#import "Utils.h"

@interface DetailNewsViewController ()

@end

@implementation DetailNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeViews];
    [self initializeInstances];
    [self setViewBorder];
    [self setDataIntoViews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self HideShowView];
      [self chanceSettingOnNightMode];
}

/*
 * Method to initialize views
 */
-(void)initializeViews{
    //Set scroll view frame
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+self.secondView.frame.size.height/4);
    
}

/*
 * Method to initialize instances
 */
-(void)initializeInstances{
    userDefaultPreferences = [UserDefaultPreferences getInstance];
}
/*
 * Method to set data into views
 */
-(void)setDataIntoViews{
    
    if (_newsDao!=nil) {
        NSString *authorName = nil;
        NSString *newsDate = nil;
        
        if (_newsDao.imageURL!=nil) {
            self.newsImageView.image = [UIImage imageNamed:_newsDao.imageURL];
        }
        
        if (_newsDao.newsHeading!=nil ) {
            NSLog(@"News Hading %@",_newsDao.newsHeading);
            self.labelNewsHeadline.text = _newsDao.newsHeading;
        }
        
        if (_newsDao.newsContent!=nil) {
            [self.detailNewsText setText:_newsDao.newsContent];
        }
        if (_newsDao.authorName!=nil) {
            authorName = [NSString stringWithFormat:@"%@", _newsDao.authorName];
            
        }
        if (_newsDao.newsDay!=nil) {
            newsDate = [NSString stringWithFormat:@"%@",_newsDao.newsDay];
        }
        
        if (authorName!=nil || newsDate!=nil) {
            [self.labelNewsDate setText:[newsDate stringByAppendingFormat:@"\n%@",authorName]];
        }
        if (_newsDao.moreNewsAt!=nil) {
            _moreAtWebText.text = _newsDao.moreNewsAt;
        }
        if (_newsDao.videoId!=nil) {
            self.youTubeVideoId = _newsDao.videoId;
        }
        
    }
    
}

/*
 * Method to change text colour and background when Night Mode is enabled/Disable
 */
-(void)chanceSettingOnNightMode{
    @autoreleasepool {
        
        if ([self isNightModeOn]) {
            self.view.backgroundColor = [UIColor blackColor];
            self.scrollView.backgroundColor = [UIColor blackColor];
            self.firstView.backgroundColor = [UIColor blackColor];
            self.secondView.backgroundColor = [UIColor blackColor];
            self.labelNewsDate.textColor = [UIColor whiteColor];
            self.labelNewsHeadline.textColor = [UIColor whiteColor];
            self.detailNewsText.textColor = [UIColor whiteColor];
            self.detailNewsText.backgroundColor = [UIColor blackColor];
            
            self.omreAtVideoView.backgroundColor = [UIColor blackColor];
            self.moreAtWebView.backgroundColor = [UIColor blackColor];
            self.moreAtParentView.backgroundColor = [UIColor blackColor];
            
            self.moreAtWebText.textColor = [UIColor blueColor];
            self.videoAtYoutubeText.textColor = [UIColor blueColor];
            self.moreAtConstantText.textColor = [UIColor whiteColor];
            self.videoAtCOnstantText.textColor = [UIColor whiteColor];
        }
        else{
            self.view.backgroundColor = [UIColor whiteColor];
            self.scrollView.backgroundColor = [UIColor whiteColor];
            self.firstView.backgroundColor = [UIColor whiteColor];
            self.secondView.backgroundColor = [UIColor whiteColor];
            self.labelNewsDate.textColor = [UIColor blackColor];
            self.detailNewsText.textColor = [UIColor darkGrayColor];
            self.labelNewsHeadline.textColor = [UIColor blackColor];
            self.detailNewsText.backgroundColor = [UIColor whiteColor];
            
            self.omreAtVideoView.backgroundColor = [UIColor whiteColor];
            self.moreAtWebView.backgroundColor = [UIColor whiteColor];
            self.moreAtParentView.backgroundColor = [UIColor whiteColor];
            
            self.moreAtWebText.textColor = [UIColor blueColor];
            self.videoAtYoutubeText.textColor = [UIColor blueColor];
            
            self.moreAtConstantText.textColor = [UIColor blackColor];
            self.videoAtCOnstantText.textColor = [UIColor blackColor];
        }
    }
    
}

/*
 * Method to set border to view only at upper side
 */
-(void)setViewBorder{
 
    [Utils setUpperViewBorder:self.moreAtParentView andBorderColour:[UIColor redColor]];
    
}


/*
 * Method to get list mode value from preferences
 */
-(BOOL)isNightModeOn{
    NSString *nightModeKey = PREF_KEY_NIGHT_MODE
    BOOL isNightModeOn = [userDefaultPreferences getBooleanData:nightModeKey];
    nightModeKey = nil;
    return isNightModeOn;
}

/*
 * Method to hide-show view as per satisfied confition
 */
-(void)HideShowView{
    
    if (self.youTubeVideoId!=nil && ![self.youTubeVideoId isEqual:@""]) {
        
        //If video is not nil then show view of youtube video
        [self.omreAtVideoView setHidden:NO];
        [self.moreAtWebView setHidden:YES];
    }
    else{
        //If video is nil then show view news link
        [self.omreAtVideoView setHidden:YES];
        [self.moreAtWebView setHidden:NO];
    }
    
}


- (IBAction)playVideoButtonClickEvent:(id)sender {
    [self playVideoInYouTube];
}

- (IBAction)moreAtWebNewsButtonClickEvent:(id)sender {
    [[ApplicationController getInstance]handleEvent:EVENT_ID_NEWS_WEB_VIEW_SCREEN andeventObject:_newsDao];
}

/*
 * Method to Play video in YouTube
 */
-(void)playVideoInYouTube{
    if (self.youTubeVideoId!=nil && ![self.youTubeVideoId isEqual:@""]) {
        NSString *string = [NSString stringWithFormat:@"http://m.youtube.com/watch?v=%@", self.youTubeVideoId];
        NSURL *url = [NSURL URLWithString:string];
        UIApplication *app = [UIApplication sharedApplication];
        [app openURL:url];
    }
    
}

-(void)destroy{
    userDefaultPreferences=nil;
    self.labelNewsDate=nil;
    self.labelNewsHeadline=nil;
    self. newsImageView=nil;
    self.detailNewsText=nil;
    self.scrollView=nil;
    self.secondView=nil;
    self.firstView=nil;
    self.moreAtParentView=nil;
    self.omreAtVideoView=nil;
    self.moreAtWebView=nil;
    self.videoAtCOnstantText=nil;
    self.videoAtYoutubeText=nil;
    self.moreAtConstantText=nil;
    self.moreAtWebText=nil;
    self.youTubeVideoId=nil;
}
@end
