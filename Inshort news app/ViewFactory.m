//
//  ViewFactory.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "ViewFactory.h"
#import "ApplicationController.h"
#import "AbstractUIView.h"
#import "AbstractViewController.h"

@implementation ViewFactory



/* Release the screen*/


/**
 * Release All Screen before exiting the application
 */
-(void)releaseAllScreen
{
    
}

-(id<AbstractViewController>) getAbstractUIViewController:(int)screenId
{
    switch (screenId) {
            
            /*
             * SAMPLE CODE ID'S
             */
      
            
        case VF_TEST_SCREEN:
        {
            self-> testViewController =[[TestScreenViewController alloc]initWithNibName:@"TestScreenViewController" bundle:nil];
            return testViewController;
        }
        case VF_ROOT_NAVIGATION_SCREEN:
        {
            self-> rootNavigationController =[RootNavigationController alloc];
            return rootNavigationController;
        }
        case VF_ROOT_NAVIGATION_CHILD_SCREEN1:
        {
            self-> child1NavigationController =[Child1NavigationController alloc];
            
            return child1NavigationController;
        }
        case VF_ROOT_NAVIGATION_CHILD_SCREEN2:
        {
            self-> child2NavigationController =[Child2NavigationController alloc];
            return child2NavigationController;
        }
        case VF_TEST1_VIEW_CONTROLLER:
        {
            self-> test1ViewController =[Test1ViewController alloc];
            return test1ViewController;
        }
        case VF_CUSTOM_TABLE_VIEW_SCREEN:
        {
            self-> customTableViewController =[CustomTableViewController alloc];
            return customTableViewController;
        }
        case VF_CUSTOM_PAGE_VIEW_SCREEN:
        {
            
            self-> customPageViewController =[[CustomPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
            
            return customPageViewController;
        }
        case VF_CUSTOM_COLLECTION_VIEW_SCREEN:
        {
            self-> customCollectionViewController =[[CustomCollectionViewController alloc]init];
            
            return customCollectionViewController;
        }
            
            
            
    }
    return nil;
}

/*
 * ACTUAL CODE ID'S
 */

-(id<AbstractViewController>) getAbstractUIViewController:(int)screenId eventObject:(NSObject *)eventObject{
    switch (screenId) {
        case VF_SPLASH_SCREEN:
        {
            SplashScreenViewController *splashScreenViewController = [[SplashScreenViewController alloc]init];
            splashScreenViewController.viewControllerType = VCT_VIEW_CONTROLLER;
            splashScreenViewController.screenId = screenId;
            return splashScreenViewController;
        }
        case VF_HOME_SCREEN:
        {
            HomeScreenViewController *homeScreenViewController = [[HomeScreenViewController alloc]init];
            homeScreenViewController.viewControllerType = VCT_VIEW_CONTROLLER;
            homeScreenViewController.screenId = screenId;
            return homeScreenViewController;
            
        }
        case VF_NEWS_WEB_VIEW_SCREEN:
        {
            NewsWebViewController *newsWebViewController = [[NewsWebViewController alloc]init];
            newsWebViewController.viewControllerType = VCT_VIEW_CONTROLLER;
            newsWebViewController.newsDo = (NewsDao*)eventObject;
            newsWebViewController.screenId = screenId;
            return newsWebViewController;
            
        }
        case VF_CATEGORIES_VIEW_SCREEN:
        {
            CateroriesViewController *cateroriesViewController = [[CateroriesViewController alloc]init];
            cateroriesViewController.viewControllerType = VCT_VIEW_CONTROLLER;
            cateroriesViewController.screenId = screenId;
            
            cateroriesViewController.homeScreenViewController = (HomeScreenViewController*)eventObject;
            return cateroriesViewController;
            
        }
        case VF_SETTING_SCREEN:
        {
            SettingViewController *settingViewController = [[SettingViewController alloc]init];
            settingViewController.viewControllerType = VCT_VIEW_CONTROLLER;
            settingViewController.screenId = screenId;
            settingViewController.homeScreenViewController = (HomeScreenViewController*)eventObject;
            return settingViewController;
            
        }
        case VF_DETAIL_NEWS_PAGER_SCREEN:
        {
            DetailNewsScreenPagerViewController *detailNewsScreenPagerViewController = [[DetailNewsScreenPagerViewController alloc]init];
            detailNewsScreenPagerViewController.viewControllerType = VCT_VIEW_CONTROLLER;
            detailNewsScreenPagerViewController.screenId = screenId;
            detailNewsScreenPagerViewController.newsDataDictionary = (NSMutableDictionary*)eventObject;
            return detailNewsScreenPagerViewController;
            
        }
            
            
    }
    
    return nil;
}
@end
