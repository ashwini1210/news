//
//  UIController.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "UIController.h"
#import "ViewController.h"
#import "ApplicationController.h"
#import "ViewFactory.h"

@implementation UIController
@synthesize UIStack;
- (id)init
{
    self = [super init];
    if (self) {
        
        self-> UIStack =  [[NSMutableArray alloc]init];
        
    }
    return self;
}


-(void)initialize
{
    
}

-(void)destory
{
    
}
-(void)popScreen:(int)viewControllerType
{
    if (currentUIViewController!=nil) {
        
        //call disable callback the current screen which is going to finish
        //        [currentUIViewController disbale];
        
        //and before removing call freeresources callback
        //        [currentUIViewController freeResources];
        
        
        //calls the destroy callback
        //        [currentUIViewController destory];
        
        //remove current view from Main View.
        //                [currentUIViewController removeFromSuperview];
        
        
        //add to main view to make this screen visible.
        switch (viewControllerType) {
            case VCT_VIEW_CONTROLLER:{
                [(UIViewController*)currentUIViewController dismissViewControllerAnimated:YES completion:^{[self onScreenPoped];}];
            }
                break;
            case VCT_ROOT_NAVIGATION_CONTROLLER:{
                [(UIViewController*)currentUIViewController dismissViewControllerAnimated:YES completion:^{[self onScreenPoped];}];
                
            }
                break;
            case VCT_CHILD_NAVIGATION_CONTROLLER:{
                [((UIViewController*)currentUIViewController).navigationController popViewControllerAnimated:YES];
                [self onScreenPoped];
            }
                break;
                
                
        }
        
    }
}


-(void) enable
{
    
}

-(void) disbale
{
    
}

-(void) hideNotify
{
    
}

-(void) showNotify
{
    
}


-(void)setRootViewController:(UIViewController*)rootUiViewController{
    self->mainRootUIViewController = (id <AbstractViewController>)rootUiViewController;
    self->currentUIViewController = mainRootUIViewController;
}

-(UIViewController*)getRootViewController{
    return (UIViewController*)self->mainRootUIViewController;
}

-(void)pushScreen:(int)viewControllerType screenId:(int)screenId viewFactory:(ViewFactory*)viewFactory
{
    UIViewController *viewController = (UIViewController*)[viewFactory getAbstractUIViewController:(screenId)];
    
    switch (viewControllerType) {
        case VCT_VIEW_CONTROLLER:
            [self pushNewScreen:(id <AbstractViewController>)viewController];
            break;
        case VCT_ROOT_NAVIGATION_CONTROLLER:{
            [self pushNewNavigationScreen:(id <AbstractViewController>)viewController];
        }
            break;
            
        case VCT_CHILD_NAVIGATION_CONTROLLER:
            [self pushChildNavigationScreen:(id <AbstractViewController>)viewController];
            
            break;
        default:
            break;
            
    }
  }


/*
 * Push screen having 4 parameters
 */
-(void)pushScreen:(int)viewControllerType screenId:(int)screenId viewFactory:(ViewFactory*)viewFactory eventObject:(NSObject *)eventObject
{
    @autoreleasepool {
        
        UIViewController *viewController = (UIViewController*)[viewFactory getAbstractUIViewController:(screenId) eventObject:eventObject];
        
        switch (viewControllerType) {
            case VCT_VIEW_CONTROLLER:{
                [self pushNewScreen:(id <AbstractViewController>)viewController];
                break;
            }
            case VCT_ROOT_NAVIGATION_CONTROLLER:{
                [self pushNewNavigationScreen:(id <AbstractViewController>)viewController];
            }
                break;
                
            case VCT_CHILD_NAVIGATION_CONTROLLER:{
                [self pushChildNavigationScreen:(id <AbstractViewController>)viewController];
            }
            break;
            default:
                break;
                
        }
        viewController = nil;
    }
    
   
}


/*
 * Pushes the new navigation screen controller initializing with rootview controller.
 */
-(void)pushNewNavigationScreen:(id<AbstractViewController>)uiViewController{
    UINavigationController *uiNavigationController = [[UINavigationController alloc] initWithRootViewController:(UIViewController*)uiViewController];
    //replace the prev UI with current UI view.
    self->prevUIViewController = self->currentUIViewController;
    
    
    //set this view as a current view.
    self->currentUIViewController = uiViewController;
    
    //remove pre view if it contains
    if (prevUIViewController!=nil) {
        //        [prevUIViewController removeFromParentViewController];
    }
    [(UIViewController*)prevUIViewController  presentViewController:uiNavigationController animated:YES completion:^{[self onNewScreenPushed];}];
    
}


/*
 * Pushes the child controller screen to navigation controller.
 */
-(void)pushChildNavigationScreen:(id<AbstractViewController>)uiViewController{
    
    //replace the prev UI with current UI view.
    self->prevUIViewController = self->currentUIViewController;
    
    
    //set this view as a current view.
    self->currentUIViewController = uiViewController;
    
    //Getting Previous Controller to get its parent navigation controller for launching next child controller.
    UIViewController *prevController = (UIViewController*)prevUIViewController;
    
    [prevController.navigationController pushViewController:(UIViewController*)currentUIViewController animated:YES];
    
    [self onNewScreenPushed];
    
}

/*
 * Launches the new normal view controller.
 */
-(void)pushNewScreen:(id<AbstractViewController>)uiViewController{
    
    //replace the prev UI with current UI view.
    self->prevUIViewController = self->currentUIViewController;
    
    
    //set this view as a current view.
    self->currentUIViewController = uiViewController;
    
    [(UIViewController*)prevUIViewController  presentViewController:(UIViewController*)currentUIViewController animated:YES completion:^{[self onNewScreenPushed];}];
    
}


/*
 This method gets call on view controller is finished, i.e poped up.
 
 */
-(void) onScreenPoped{
    //calls the reInitialize callback for prev screen which is going to be in foreground.
    //    [prevUIViewController reInitialize];
    
    //remove the screen which need to be finished and making prev screen to front and top at stack.
    [UIStack removeObject:currentUIViewController];
    
    //calls the enable callback for prev screen.
    //    [prevUIView enable];
    
    NSLog(@"After Current UI Removed: %u",[UIStack count]);
    
    //here calling the destroy method for removed view controller.
    [currentUIViewController destroy];
    
    id<AbstractViewController> tempCurrentViewController = currentUIViewController;
    
    currentUIViewController = prevUIViewController;
    
    NSInteger prevViewInd = [UIStack count]-2;
    
    if(prevViewInd>=0){
        prevUIViewController = [UIStack objectAtIndex:prevViewInd];
    }
    [tempCurrentViewController onScreenPopedUp];

    [currentUIViewController onTopScreenFinished];
    
    tempCurrentViewController = nil;
    
    NSLog(@"After Pop Up : %u",[UIStack count]);
}
/*
 This method gets call on view controller is added, i.e pushed.
 
 */
-(void) onNewScreenPushed{
    //call the initialize call back of current UIView
    //    [currentUIViewController initialize];
    
    
    //call enable callback of current UIView as it set to the Main View.
    //    [currentUIViewController enable];
    
    
    //add this view to stack at the top position.
    [self->UIStack addObject:currentUIViewController];
    NSLog(@"Stack Size: %u",[UIStack count]);
}

/*
 * Method to release UI controller stack on logout
 */
-(void)clearUIStackToScreen:(int)screenId{
    @autoreleasepool {
        UIViewController *presentingUIViewController = nil;
        
        for (int i=[UIStack count]-1; i>=0; i--) {
            
            NSLog(@"UI stack count %lu",(unsigned long)[UIStack count]);
            
            BaseViewController *abstractViewController = [UIStack objectAtIndex:i];
            NSLog(@"Controller >> %@",abstractViewController);
            
            if(abstractViewController.screenId==screenId){
                //here if the above provided screen id matched, then stopping looping & returning back.
                if (presentingUIViewController!=nil) {
                    [presentingUIViewController dismissViewControllerAnimated:NO completion:^{[self removeScreensFromStackToScreen:screenId];}];
                }
                return;
            }
            if(abstractViewController.viewControllerType== VCT_VIEW_CONTROLLER){
               
                if (presentingUIViewController==nil) {
                presentingUIViewController = ((UIViewController*)currentUIViewController);
                }
            }
            presentingUIViewController = presentingUIViewController.presentingViewController;
            NSLog(@"View Controller Type in UI Stack %d",abstractViewController.viewControllerType);
        }
    }
}

/*
 * Removes the screens objects from stack as those are already not part of the UI.
 */
-(void)removeScreensFromStackToScreen:(int)screenId{
    NSLog(@"removeScreensFromStackToScreen");
    //until currentview controller will be similar to provided screen id, loop will be continue to pop screen.
    for(;((BaseViewController*)currentUIViewController).screenId!=screenId;){
        [self onScreenPoped];
    }
}
@end
