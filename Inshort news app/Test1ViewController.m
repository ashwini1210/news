//
//  Test1ViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 10/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "Test1ViewController.h"
#import "CustomPageViewController.h"
@interface Test1ViewController ()

@end

@implementation Test1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goToNextScreenButtonClicked:(id)sender {
    
    [[ApplicationController getInstance] handleEvent:EVENT_ID_TEST_SCREEN];
//    [self presentViewController:[[CustomPageViewController alloc]init] animated:YES completion:nil];
}

- (IBAction)backButtonPressed:(id)sender {
    NSLog(@"On Test Screen Back Button");
    [[ApplicationController getInstance] handleEvent:EVENT_ID_FINISH_SCREEN];
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    NSLog(@"On Test Screen moved to parent");
    if (![parent isEqual:self.parentViewController]) {
//        [[ApplicationController getInstance] handleEvent:EVENT_ID_ON_SCREEN_FINISH];
        NSLog(@"Back pressed");
    }
}
@end
