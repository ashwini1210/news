//
//  Utils.m
//  IosApplicationFrameworkProject
//
//  Created by test on 16/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "Utils.h"
#import "AppConstants.h"


static UIButton*  previousSelectedButton;
static UIImageView* previousSelectedImageView;


@implementation Utils

/*
 * Method to convert interger hexadecimal value in UIColor format.
 */
+ (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}


/*
 * Method which works as helper method to convert String Hexadecimal Value into Interger format.
 */
+ (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}


/**
 * Method to give shadow to view.
 **/
+ (void)showCenterViewWithShadow:(BOOL)value withOffset:(double)offset view:(UIView *)containerView
{
    
    if (value)
    {
        [containerView.layer setCornerRadius:4];
        [containerView.layer setShadowColor:[UIColor blackColor].CGColor];
        [containerView.layer setShadowOpacity:0.8];
        [containerView.layer setShadowOffset:CGSizeMake(offset, offset)];
        
    }
    else
    {
        [containerView.layer setCornerRadius:0.0f];
        [containerView.layer setShadowOffset:CGSizeMake(offset, offset)];
    }
}


/*
 * Method to set border to view.
 */
+(void)setBorderToView:(UIView*)view{
    
    view.layer.cornerRadius = 5.0f; // set as you want.
    view.layer.borderColor = [UIColor darkGrayColor].CGColor; // set color as you want.
    view.layer.borderWidth = 1.0;
}

/*
 * Method to set border to view only at upper side
 */
+(void)setUpperViewBorder:(UIView*)view andBorderColour:(UIColor*)colour{
    
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [colour CGColor];
   // upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame), 1.0f);
    upperBorder.frame = CGRectMake(0, 0, view.frame.size.width*1.3, 1.0f);
   // NSLog(@"View width %f",view.frame.size.width);
    [view.layer addSublayer:upperBorder];
    
}


/*
 * Method to convert string into camel case.
 */
+(NSString *)camelCaseFromString:(NSString *)input{
    
    return[input capitalizedString];
}

/*
 * Change color of selected button and also image size.
 */
+(void)changeSelectedViewColor:(UIButton*)selectedButton andImageView:(UIImageView*)selectedImageView unSelectedImagesArray:(NSArray*)unSelectedImagesArray selectedImagesArray:(NSArray*)selectedImagesArray{
    
    [self deselectPreviousView:unSelectedImagesArray];
    [self selectCurrentView:selectedButton andImageView:selectedImageView selectedImagesArray:selectedImagesArray];
}

/*
 * Method to clear property of previously selected view
 */
+(void)deselectPreviousView:(NSArray*)unSelectedImagesArray{
    
    //Remove background of previously selected button
    if (previousSelectedButton!=nil) {
        int previousButtonIndex = (int)previousSelectedButton.tag;
        [previousSelectedButton setBackgroundColor:[UIColor clearColor]];
        previousSelectedImageView.image = [UIImage imageNamed:unSelectedImagesArray[previousButtonIndex]];
        previousButtonIndex = 0;
    }
}

/*
 * Method to set property of selected view
 */
+(void)selectCurrentView:(UIButton*)selectedButton andImageView:(UIImageView*)selectedImageView selectedImagesArray:(NSArray*)selectedImagesArray{
    //Maintain index of selected
    int selectedButtonIndex = (int)selectedButton.tag;
    
    //Set background of selected button
    UIColor *appThemeOrangeColor = [Utils getUIColorObjectFromHexString:APP_THEME_LIGHT_ORANGE_COLOR alpha:0.3];
    [selectedButton setBackgroundColor:appThemeOrangeColor];
    selectedImageView.image = [UIImage imageNamed:selectedImagesArray[selectedButtonIndex]];
    
    //Assign value of selected button to previously selected buuton
    previousSelectedButton = selectedButton;
    previousSelectedImageView = selectedImageView;
    
    
    selectedButtonIndex = 0;
    appThemeOrangeColor = nil;
    
}

/*
 * logic of share data
 */
+(void)socialShareData:(NSString*)shareString andShareImage:(UIImage *)shareImage andShareURL:(NSURL *)shareUrl andPresentViewController:(UIViewController*)presentViewController{
    
    UIActivityViewController *activityViewController;
    NSArray *activityItems;
    
    if (shareString!=nil && shareImage!=nil && shareUrl!=nil) {
        activityItems = [NSArray arrayWithObjects:shareString, shareImage, shareUrl, nil];
    }
    
    // build an activity view controller
    activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    //activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    //for sharing only,the options for saving to camera roll, printing and copying do not apply. Following line of code will remove those options from the list.
     activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    
    //If device is iphone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [presentViewController presentViewController:activityViewController animated:YES completion:nil];
    }
    
    //If device is iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(presentViewController.view.frame.size.width/2, presentViewController.view.frame.size.height/4, 0, 0)inView:presentViewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
    [activityViewController setCompletionHandler:^(NSString *act, BOOL done)
     {
         NSString *ServiceMsg = nil;
         if ( [act isEqualToString:UIActivityTypeMail] )           ServiceMsg = @"Mail sended!";
         if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"Post on twitter, ok!";
         if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"Post on facebook, ok!";
         if ( [act isEqualToString:UIActivityTypeMessage] )        ServiceMsg = @"SMS sended!";
         if ( done )
         {
             UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
             [Alert show];
             
         }
         ServiceMsg = nil;
         
     }];
    activityItems = nil;
    activityViewController = nil;
}

/*
 * Method to get dao from selected Category
 */
+(NewsDao*)getDaoFromCategory:(NSString*)selectedCategory andNewsIndex:(int)pageIndex{
    
    LocalModel *localModel = [[[ApplicationController getInstance] getModelFacade] getLocalModel];
    NewsListModel *newsListModel = localModel.newsListModel;
    NewsDao *newsDao;
    @autoreleasepool {
        if ([selectedCategory isEqual:TOP_STORIES_CATEGORY_TYPE]) {
            newsDao = [newsListModel.topStoryDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        else if ([selectedCategory isEqual:ALL_NEWS_CATEGORY_TYPE]){
            newsDao = [newsListModel.allNewsDataArray objectAtIndex:pageIndex];
            return newsDao;
        }
        else if ([selectedCategory isEqual:TRENDING_CATEGORY_TYPE]){
            newsDao = [newsListModel.trendingDataArray objectAtIndex:pageIndex];
            return newsDao;
        }
        else if ([selectedCategory isEqual:INDIA_CATEGORY_TYPE]){
            newsDao = [newsListModel.indiaDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        
        else if ([selectedCategory isEqual: BUISNESS_CATEGORY_TYPE]){
            newsDao = [newsListModel.buisnessDataArray objectAtIndex:pageIndex];
            return newsDao;
        }
        
        else if ([selectedCategory isEqual:POLITICS_CATEGORY_TYPE]){
            newsDao = [newsListModel.politicsDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        
        else if ([selectedCategory isEqual: SPORTS_CATEGORY_TYPE]){
            newsDao = [newsListModel.sportsDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        
        else if ([selectedCategory isEqual: TECHNOLOGY_CATEGORY_TYPE]){
            newsDao = [newsListModel.technologyDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        else if ([selectedCategory isEqual:STARTUP_CATEGORY_TYPE]){
            newsDao = [newsListModel.startupDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        
        else if ([selectedCategory isEqual:ENTERTAINMENT_CATEGORY_TYPE]){
            newsDao = [newsListModel.entertainmentDataArray objectAtIndex:pageIndex];
            return newsDao;
            
        }
        
        else if ([selectedCategory isEqual:INTERNATIONAL_CATEGORY_TYPE]){
            newsDao = [newsListModel.internationalDataArray objectAtIndex:pageIndex];
            return newsDao;
        }
        else if ([selectedCategory isEqual:HEALTH_CATEGORY_TYPE]){
            newsDao = [newsListModel.healthDataArray objectAtIndex:pageIndex];
            return newsDao;
        }
        else if ([selectedCategory isEqual:UNREAD_CATEGORY_TYPE]){
            
            newsDao = [newsListModel.unreadNewsArray objectAtIndex:pageIndex];
            return newsDao;
        }
        else{
            newsDao = [newsListModel.allNewsDataArray objectAtIndex:pageIndex];
            return newsDao;
        }
    }
    return newsDao;
    
}


/*
 * Method to get dao from selected Category
 */
+(NSMutableArray*)getArrayFromCategory:(NSString*)selectedCategory{
    
    LocalModel *localModel = [[[ApplicationController getInstance] getModelFacade] getLocalModel];
    NewsListModel *newsListModel = localModel.newsListModel;
    
    @autoreleasepool {
        if ([selectedCategory isEqual:TOP_STORIES_CATEGORY_TYPE]){
            return newsListModel.topStoryDataArray;
        }
        else if ([selectedCategory isEqual:ALL_NEWS_CATEGORY_TYPE]){
            return newsListModel.allNewsDataArray;
        }
        else if ([selectedCategory isEqual:TRENDING_CATEGORY_TYPE]){
            return newsListModel.trendingDataArray;
        }
        else if ([selectedCategory isEqual:INDIA_CATEGORY_TYPE]){
            return newsListModel.indiaDataArray;
        }
        else if ([selectedCategory isEqual:BUISNESS_CATEGORY_TYPE]){
            return newsListModel.buisnessDataArray;
        }
        else if ([selectedCategory isEqual:POLITICS_CATEGORY_TYPE]){
            return newsListModel.politicsDataArray;
        }
        else if ([selectedCategory isEqual:SPORTS_CATEGORY_TYPE]){
            return newsListModel.sportsDataArray;
        }
        else if ([selectedCategory isEqual:TECHNOLOGY_CATEGORY_TYPE]){
            return newsListModel.technologyDataArray;
        }
        else if ([selectedCategory isEqual:STARTUP_CATEGORY_TYPE]){
            return newsListModel.startupDataArray;
        }
        else if ([selectedCategory isEqual:ENTERTAINMENT_CATEGORY_TYPE]){
            return newsListModel.entertainmentDataArray;
        }
        else if ([selectedCategory isEqual:INTERNATIONAL_CATEGORY_TYPE]){
            return newsListModel.internationalDataArray;
        }
        else if ([selectedCategory isEqual:HEALTH_CATEGORY_TYPE]){
            return newsListModel.healthDataArray;
        }
        else if ([selectedCategory isEqual:UNREAD_CATEGORY_TYPE]){
            return newsListModel.unreadNewsArray;
        }
        else if ([selectedCategory isEqual:BOOKMARK_CATEGORY_TYPE]){
            return newsListModel.bookMarkArray;
        }
        return newsListModel.allNewsDataArray;
        
    }
}

/*
 * Take screen shot of current page
 */
+(void)takePageScreenShots:(UIView*)view andPresentViewController:(UIViewController*)presentViewController{
    
    UIImage *image;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]){
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
    }
    else{
        UIGraphicsBeginImageContext(view.bounds.size);
    }
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData * imgData = UIImagePNGRepresentation(image);
    if(imgData)
    {
        [imgData writeToFile:@"screenshot.png" atomically:YES];
    }
    else{
        NSLog(@"error while taking screenshot");
    }
    
    [self shareData:image andPresentViewController:presentViewController];
    
}

/*
 * logic of share data
 */
+(void)shareData:(UIImage*)image andPresentViewController:(UIViewController*)presentViewController{
    NSURL *urlString = [NSURL URLWithString:@"http://www.pollyannaish.com"];
    [self socialShareData:@"\"Pollyannaish\" News App" andShareImage:image andShareURL:urlString andPresentViewController:presentViewController];
    
}




@end
