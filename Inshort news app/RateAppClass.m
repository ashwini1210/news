//
//  RateAppClass.m
//  IosApplicationFrameworkProject
//
//  Created by test on 25/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "RateAppClass.h"

@implementation RateAppClass

-(id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

/*
 * initialize class
 */
-(id)initialize:(SettingViewController*)settingViewController{
    
    settingVController = settingViewController;
   [self initializeInstances];
    [self checkAppRates];

    return self;
}

/*
 * Method to initialize instances
 */
-(void)initializeInstances{
     
    }


/*
 * It checks to see how many times the user has launched the app and increments it by 1. If the launch counter is equal to 3, 9, 15, or 21 we’ll show the Rate App alert message unless “neverRate” equals to YES.
 */
-(void)checkAppRates{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //Ask for Rating
    BOOL neverRate = [prefs boolForKey:@"neverRate"];
    
    int launchCount = 0;
    
    //Get the number of launches
    launchCount = [prefs integerForKey:@"launchCount"];
    launchCount++;
    [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
    
    if (!neverRate)
    {
        [self rateApp];
        
    }
    [prefs synchronize];
}

/*
 * Rate app
 */
- (void)rateApp {
    BOOL neverRate = [[NSUserDefaults standardUserDefaults] boolForKey:@"neverRate"];
    
    if (neverRate != YES) {
        [settingVController.customAlertViewWithThreeButtons show:settingVController];
    }
}

//Rate app button
-(void)onAlertViewLeftButtonClicked:(int)identifier{
   // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.google.co.in/?gfe_rd=cr&ei=sQL1VvDZI6zT8ge1yI_oAg&gws_rd=ssl"]];
}

//Remind me later button
-(void)onAlertViewCenterButtonClicked:(int)identifier{
    NSLog(@"Clicked on REMIND ME LATER button");
}

//No thanks button
-(void)onAlertViewRightButtonClicked:(int)identifier{
    NSLog(@"Clicked on NO,THANKS button");
   // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
}



//IT'S ORIGINAL CODE OF RATE APP USING IRATE, GOT FORM SITE

//-(void)checkAppRates{
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    //Ask for Rating
//    BOOL neverRate = [prefs boolForKey:@"neverRate"];
//
//    int launchCount = 0;
//    //Get the number of launches
//    launchCount = [prefs integerForKey:@"launchCount"];
//    launchCount++;
//    [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
//
//    if (!neverRate)
//    {
//
//                if ( (launchCount == 3) || (launchCount == 9) || (launchCount == 15) || (launchCount == 21) )
//                {
//                    [self rateApp];
//                }
//    }
//    [prefs synchronize];
//}
//- (void)rateApp {
//    BOOL neverRate = [[NSUserDefaults standardUserDefaults] boolForKey:@"neverRate"];
//
//    if (neverRate != YES) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please rate <APP NAME>!"
//                                                        message:@"If you like it we'd like to know."
//                                                       delegate:self
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"Rate It Now", @"Remind Me Later", @"No, Thanks", nil];
//        alert.delegate = self;
//        [alert show];
//    }
//}
//
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 0) {
//        NSLog(@"Clicked on RATE IT NOW button");
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.google.co.in/?gfe_rd=cr&ei=sQL1VvDZI6zT8ge1yI_oAg&gws_rd=ssl"]];
//    }
//
//    else if (buttonIndex == 1) {
//         NSLog(@"Clicked on REMIND ME LATER button");
//    }
//
//    else if (buttonIndex == 2) {
//         NSLog(@"Clicked on NO,THANKS button");
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
//    }
//}


@end
