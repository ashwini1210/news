//
//  ViewFactory.h
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#ifndef __VIEW_FACTORY_H__
#define __VIEW_FACTORY_H__

//Sample App IMPORTS
#import <Foundation/Foundation.h>
#import "AppConstants.h"

#import "MainScreen.h"
#import "TestScreenViewController.h"
#import "RootNavigationController.h"
#import "Child1NavigationController.h"
#import "Child2NavigationController.h"
#import "Test1ViewController.h"
#import "CustomTableViewController.h"
#import "CustomPageViewController.h"
#import "CustomCollectionViewController.h"

//Actual App IMPORTS
#import "HomeScreenViewController.h"
#import "NewsWebViewController.h"
#import "CateroriesViewController.h"
#import "SettingViewController.h"
#import "DetailNewsScreenPagerViewController.h"
#import "SplashScreenViewController.h"



@interface ViewFactory : NSObject
{
    
    MainScreen *mainScreen;
    TestScreenViewController *testViewController;
    RootNavigationController *rootNavigationController;
    Child1NavigationController *child1NavigationController;
    Child2NavigationController *child2NavigationController;
    Test1ViewController *test1ViewController;
    CustomTableViewController *customTableViewController;
    CustomPageViewController *customPageViewController;
    CustomCollectionViewController *customCollectionViewController;
}


-(void)releaseAllScreen;
-(id<AbstractViewController>) getAbstractUIViewController:(int)screenId;
-(id<AbstractViewController>) getAbstractUIViewController:(int)screenId eventObject:(NSObject *)eventObject;


@end



#endif