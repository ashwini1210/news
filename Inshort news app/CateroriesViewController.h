//
//  CateroriesViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 14/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Utils.h"
#import "HomeScreenViewController.h"
#import "LocalModel.h"
#import "NewsListModel.h"


@interface CateroriesViewController : BaseViewController
{
    BOOL needToSetScrollViewSize;
    NSArray *selectedImagesArray;
    NSArray *unSelectedImagesArray;
    LocalModel *localModel;
    NewsListModel *newsListModel;
   
}


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *allNewsView;
@property (strong, nonatomic) IBOutlet UIView *topStoriesView;
@property (strong, nonatomic) IBOutlet UIView *trendingNewsView;
@property (strong, nonatomic) IBOutlet UIView *bookMarkNewsView;
@property (strong, nonatomic) IBOutlet UIView *unreadNewsView;
@property (strong, nonatomic) IBOutlet UIView *moreNewsView;
@property (strong, nonatomic) IBOutlet UIView *secondParentLayoutView;
@property (strong, nonatomic) IBOutlet UIView *indianNewsView;
@property(strong,nonatomic) UIButton* previousSelectedButton;
@property(strong,nonatomic) UIImageView* previousSelectedImageView;


@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *allNewsButton;
- (IBAction)allNewsButtonClcikEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *topStoriesButton;
- (IBAction)topStoriesButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *trendingButton;
- (IBAction)trendingButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bookmarkButton;
- (IBAction)bookmarkButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *unreadButton;
- (IBAction)unreadButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *indiaButton;
- (IBAction)indiaButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *moreButton;
- (IBAction)moreButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *buisnessButton;
- (IBAction)buisnessButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *politicsButton;
- (IBAction)politicsButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sportsButton;
- (IBAction)sportsButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *technologyButton;
- (IBAction)technologyButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *startUpButton;
- (IBAction)startUpButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *entertainmentButton;
- (IBAction)entertainmentButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *internationalButton;
- (IBAction)internationalButtonClickButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *healthButton;
- (IBAction)healthButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *lessButton;
- (IBAction)lessButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *firstParentView;




@property (strong, nonatomic) IBOutlet UIImageView *allNewsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *topStoriesImageView;
@property (strong, nonatomic) IBOutlet UIImageView *trendingImageView;
@property (strong, nonatomic) IBOutlet UIImageView *bookmarkImageView;
@property (strong, nonatomic) IBOutlet UIImageView *unreadImageView;
@property (strong, nonatomic) IBOutlet UIImageView *moreImageView;
@property (strong, nonatomic) IBOutlet UIImageView *indiaImageView;
@property (strong, nonatomic) IBOutlet UIImageView *buisnessImageView;
@property (strong, nonatomic) IBOutlet UIImageView *politicsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sportsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *technologyImageView;
@property (strong, nonatomic) IBOutlet UIImageView *startUpImageView;
@property (strong, nonatomic) IBOutlet UIImageView *entertainmentImageView;
@property (strong, nonatomic) IBOutlet UIImageView *internationalImageViews;
@property (strong, nonatomic) IBOutlet UIImageView *healthImageView;
@property (strong, nonatomic) IBOutlet UIImageView *lessImageView;

@property (strong, nonatomic) HomeScreenViewController *homeScreenViewController;


@end
