//
//  AppConstants.m
//  IosApplicationFrameworkProject
//
//  Created by Ranjit singh on 2/9/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "AppConstants.h"

@implementation AppConstants
/*
 * Server URL i.e. Host URL.
 */
NSString* const SERVER_URL =  @"http://172.16.1.162:8080/";
/*
 * Sign in URL.
 */
NSString* const URL_CHECK_SIGN_IN= @"http://172.16.1.162:8080/users";
/*
 * Sign up  URL i.e. Host URL.
 */
NSString* const URL_CHECK_SIGN_UP= @"http://172.16.1.162:8080/users/signin";
/*
 * Verification  URL i.e. Host URL.
 */
NSString* const URL_CHECK_VERIFICATION= @"http://172.16.1.162:8080/users/verification";
/*
 * Url to retrieve all users.
 */
NSString* const URL_CHECK_GET_ALL_USERS = @"http://172.16.1.162:8080/users";

// 3.0 seconds splash screen time
int const SPLASH_DISPLAY_LENGTH= 3.0;
int const SUCCESS_CODE = -1;
int const ALL_NEWS_BUTTON_TAG=0;
int const TOP_STORIES_BUTTON_TAG=1;
int const TRENDING_BUTTON_TAG=2;
int const BOOKMARK_BUTTON_TAG=3;
int const UNREAD_BUTTON_TAG=4;
int const MORE_BUTTON_TAG=5;
int const INDIA_BUTTON_TAG=6;
int const BUISNESS_BUTTON_TAG=7;
int const POLITICS_BUTTON_TAG=8;
int const SPORTS_BUTTON_TAG=9;
int const TECHNOLOGY_BUTTON_TAG=10;
int const STARTUP_BUTTON_TAG=11;
int const ENTERTAINMENT_BUTTON_TAG=12;
int const INTERNATIONAL_BUTTON_TAG=13;
int const HEALTH_BUTTON_TAG=14;
int const LESS_BUTTON_TAG=15;
int const NOTIFICATION_BUTTON_TAG=0;
int const HD_IMAGE_BUTTON_TAG=1;
int const NIGHT_MODE_BUTTON_TAG=2;
int const LIST_MODE_BUTTON_TAG=3;
int const SHARE_APP_BUTTON_TAG=4;
int const RATE_APP_BUTTON_TAG=5;
int const FEEDBACK_BUTTON_TAG=6;
int const TERM_CONDITION_BUTTON_TAG=7;
int const PRIVACY_BUTTON_TAG=8;

NSString* const CUSTOM_ALERT_ERROR_TITLE = @"Alert";
NSString* const CUSTOM_ALERT_COMMING_SOON_MESSAGE = @"Coming Soon !";
NSString* const CUSTOM_ALERT_CANCEL_BUTTON_TEXT =@"Cancel";
NSString* const CUSTOM_ALERT_YES_BUTTON_TEXT =@"Yes";
NSString* const CUSTOM_ALERT_NO_BUTTON_TEXT =@"No";
NSString* const CUSTOM_ALERT_OK_BUTTON_TEXT =@"Ok";
NSString* const CUSTOM_ALERT_SUCCESS_MESSAGE =@"Success";
NSString* const PLEASE_WAIT = @"Please wait...";
NSString* const ALL_NEWS_CATEGORY_TYPE=@"100" ;
NSString* const TOP_STORIES_CATEGORY_TYPE= @"101";
NSString* const TRENDING_CATEGORY_TYPE=@"102";
NSString* const BOOKMARK_CATEGORY_TYPE=@"103";
NSString* const UNREAD_CATEGORY_TYPE=@"104";
NSString* const MORE_CATEGORY_TYPE=@"105";
NSString* const INDIA_CATEGORY_TYPE=@"106";
NSString* const BUISNESS_CATEGORY_TYPE=@"107";
NSString* const POLITICS_CATEGORY_TYPE=@"108";
NSString* const SPORTS_CATEGORY_TYPE=@"109";
NSString* const TECHNOLOGY_CATEGORY_TYPE=@"110";
NSString* const STARTUP_CATEGORY_TYPE=@"111";
NSString* const ENTERTAINMENT_CATEGORY_TYPE=@"112";
NSString* const INTERNATIONAL_CATEGORY_TYPE=@"113";
NSString* const HEALTH_CATEGORY_TYPE=@"114";
NSString* const LESS_CATEGORY_TYPE=@"115";


/*FEEDBACK SCREEN VIEW CONTROLLER CONSTANTS*/
NSString *const FEEDBACK_SCREEN_MAIL_TITLE = @"Feedback for Pollyannaish iOS app";
NSString *const FEEDBACK_SCREEN_MAIL_SAVED = @"Draft Saved";
NSString *const FEEDBACK_SCREEN_MAIL_FAILED =@"Failed";
NSString *const FEEDBACK_SCREEN_MAIL_SENT = @"Success";
NSString *const FEEDBACK_SCREEN_MAIL_SAVED_MESSAGE = @"Composed Mail is saved in draft.";
NSString *const FEEDBACK_SCREEN_MAIL_FAILED_MESSAGE = @"Sorry! Failed to send.";
NSString *const FEEDBACK_SCREEN_MAIL_SENT_MESSAGE = @"Thank you for your feedback.";

/*SETTING SCREEN VIEW CONTROLLER CONSTANTS*/
NSString *const SETTING_SCREEN_COMING_SOON_MESSAGE = @"Coming Soon";


/*DETAIL LIST PAGER VIEW CONTROLLER */
NSString *const SELECTED_CELL_INDEX_KEY = @"SELECTED_CELL_INDEX";
NSString *const SELECTED_CATEGORY_ARRAY = @"SELECTED_CATEGORY_ARRAY";

/*HOME SCREEN PAGER VIEW CONTROLLER */
NSString *const NO_SELECTED_CATEGORY = @"No news with selected category";
NSString *const NO_SELECTED_BOOKMARK = @"No Bookmarks!\nTap on the title\nto bookmark a story";

/*NOTIFICATION KEYS*/
NSString *const NOTIFICATION_NEWS_KEY = @"notification_News_Key";
NSString *const NOTIFICATION_KEY_NEWS_ID = @"notification_news_id";
NSString *const NOTIFICATION_KEY_NEWS_HEADING =  @"notification_news_heading";
NSString *const  NOTIFICATION_KEY_NEWS_DAO = @"notification_news_dao";
@end
