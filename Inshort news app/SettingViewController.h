//
//  SettingViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 18/03/16.
//  Copyright (c) 2016 syslogic. All rights reserved.
//

#import "BaseViewController.h"
#import "Utils.h"
#import "AppConstants.h"
#import "UserDefaultPreferences.h"
#import <MessageUI/MessageUI.h>
#import "CustomAlertView.h"
#import "HomeScreenViewController.h"
#import "Utils.h"
#import "NotificationClass.h"
#import "LocalModel.h"
#import "ApplicationController.h"

@class FeedbackAppClass;
@class RateAppClass;


@interface SettingViewController : BaseViewController<MFMailComposeViewControllerDelegate,CustomAlertViewDelegate>

{
    NSArray *selectedImagesArray;
    NSArray *unSelectedImagesArray;
    UserDefaultPreferences *userDefaultPreferences;
    NotificationClass *notificationClass;
    LocalModel *localModel;
}

@property (strong, nonatomic) IBOutlet UIScrollView *SCROLLVIEW;
@property (strong, nonatomic) IBOutlet UISwitch *notificationSwitch;
- (IBAction)notificationSwitch:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *hdImageSwitch;
- (IBAction)hdImageSwitch:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *nightModeSwitch;
- (IBAction)nightModeSwitch:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *notificationImageView;
@property (strong, nonatomic) IBOutlet UIImageView *hdImageView;
@property (strong, nonatomic) IBOutlet UIImageView *nightModeImageView;
@property (strong, nonatomic) IBOutlet UIImageView *shareAppImageView;
@property (strong, nonatomic) IBOutlet UIImageView *rateAppImageView;
@property (strong, nonatomic) IBOutlet UIImageView *feedBackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *termConditionImageView;
@property (strong, nonatomic) IBOutlet UIImageView *privacyImageView;

@property (strong, nonatomic) IBOutlet UIButton *shareAppButton;
- (IBAction)shareAppButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *rateAppButton;
- (IBAction)rateAppButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *feedBackButton;
- (IBAction)feedBackButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *termConditionButton;
- (IBAction)termConditionButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *privacyButton;
- (IBAction)privacyButtonClickEvent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *returnButton;
- (IBAction)returnButtonClickEvent:(id)sender;

@property(strong,nonatomic) UIButton* previousSelectedButton;
@property(strong,nonatomic) UIImageView* previousSelectedImageView;
@property(strong,nonatomic)  FeedbackAppClass *feedbackAppClass;
@property (strong,nonatomic) RateAppClass *rateAppClass;
@property (nonatomic, weak) id <MFMailComposeViewControllerDelegate> mFMailComposeViewControllerDelegate;
@property(strong,nonatomic) MFMailComposeViewController *mailViewController;
@property(strong,nonatomic) CustomAlertView *customAlertViewWithThreeButtons;
@property(strong,nonatomic) CustomAlertView *customAlertView;
@property (strong, nonatomic) IBOutlet UIImageView *listModeImageView;
@property (strong, nonatomic) IBOutlet UISwitch *listModeSwitch;
- (IBAction)listModeSwitch:(id)sender;

@property (strong,nonatomic) HomeScreenViewController *homeScreenViewController;


@end
