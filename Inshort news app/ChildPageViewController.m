//
//  ChildPageViewController.m
//  IosApplicationFrameworkProject
//
//  Created by test on 14/07/15.
//  Copyright (c) 2015 syslogic. All rights reserved.
//

#import "ChildPageViewController.h"
#import "ApplicationController.h"
@interface ChildPageViewController ()

@end

@implementation ChildPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSBundle mainBundle] loadNibNamed:@"ChildPageViewController" owner:self options:nil];
    
    self.titleLabel.text = [NSString stringWithFormat:@"Custom Page View Controller %ld", (long)self.index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goToNextScreenButtonClicked:(id)sender {
    
    [[ApplicationController getInstance]handleEvent:EVENT_ID_CUSTOM_TABLE_VIEW_SCREEN];
}

- (IBAction)backButtonPressed:(id)sender {
    NSLog(@"On Page Back Button Clicked");
    [[ApplicationController getInstance]handleEvent:EVENT_ID_FINISH_SCREEN];
}
@end
