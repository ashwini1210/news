//
//  SplashScreenViewController.h
//  IosApplicationFrameworkProject
//
//  Created by test on 18/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SplashScreenViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UIImageView *splashBackgroundImageView;

@end
