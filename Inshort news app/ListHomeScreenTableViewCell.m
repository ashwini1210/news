//
//  ListHomeScreenTableViewCell.m
//  IosApplicationFrameworkProject
//
//  Created by test on 01/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import "ListHomeScreenTableViewCell.h"


@implementation ListHomeScreenTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self chanceSettingOnNightMode];
    
}

/*
 * Method to change text colour and background when Night Mode is enabled/Disable
 */
-(void)chanceSettingOnNightMode{
    
    if ([self isNightModeOn]) {
        self.contentViews.backgroundColor = [UIColor blackColor];
        self.newsDateLabel.backgroundColor = [UIColor blackColor];
        self.newsDateLabel.textColor = [UIColor whiteColor];
        self.newsHeadlinesLabel.textColor = [UIColor whiteColor];
        self.newsHeadlinesLabel.backgroundColor = [UIColor blackColor];
        self.newsImageView.backgroundColor = [UIColor blackColor];
        
    }
    else{
        self.contentViews.backgroundColor = [UIColor whiteColor];
        self.newsDateLabel.backgroundColor = [UIColor whiteColor];
        self.newsDateLabel.textColor = [UIColor blackColor];
        self.newsHeadlinesLabel.textColor = [UIColor blackColor];
        self.newsHeadlinesLabel.backgroundColor = [UIColor whiteColor];
        self.newsImageView.backgroundColor = [UIColor whiteColor];
    }
    
}

/*
 * Method to get night mode value from preferences
 */
-(BOOL)isNightModeOn{
    userDefaultPreferences = [UserDefaultPreferences getInstance];
    NSString *nightModeKey = PREF_KEY_NIGHT_MODE;
    BOOL iNightModeOn = [userDefaultPreferences getBooleanData:nightModeKey];
    nightModeKey = nil;
    return iNightModeOn;
}

@end
