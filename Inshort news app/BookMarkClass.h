//
//  BookMarkClass.h
//  IosApplicationFrameworkProject
//
//  Created by test on 05/04/16.
//  Copyright © 2016 syslogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeScreenViewControllerAdapters.h"
#import "LocalModel.h"
#import "ApplicationController.h"
#import "NewsListModel.h"
#import "Utils.h"

@interface BookMarkClass : NSObject
{

    HomeScreenViewControllerAdapters *homeScreenViewControllerAdapters;
    LocalModel *localModel;
    NewsListModel *newsListModel;

}
@property int pageIndex;
-(id)init:(HomeScreenViewControllerAdapters*)homeScreenAdapters;
-(void)initialize;
-(void)setBookMarkBackgroundColour;
-(void)setDefaultBackgroundColour;
-(void)setBookMarkNews;
-(void)removeBookMarkNews;
@end
