# Pollyannaish

## Summery

A news app that shows all the latest news and displays full content on a detail page. Also watch video on Youtube.

## Version

This app is developed for iPhone using **Objective-C**

## Build and Runtime Requirements

1. Xcode 10.0 or later
2. iOS 12 or later
3. OS X v10.14 or later

## Configuring the Project

Open Pollyannaish.xcodeproj in Xcode


## How it works:

![](https://media.giphy.com/media/cqekAk7DQXY4BTGV2x/giphy.gif)


## Features:

1. Listing of all the news.
2. Can watch videos on Youtube or read detailed news on the news website.
3. Multiple categories to display news like Top stores, Trending, Bookmark, unread, etc.
4. Settings of Notification, HD images, Night Mode, and List Mode.
5. Share app, Rate app, and also give feedback to the app.



